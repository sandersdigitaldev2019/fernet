$mobile = 1025;
$site = window.location.origin;
$email_cliente = localStorage.getItem('email_cliente');
$id_escola = localStorage.getItem('id_escola');

$('#chave_escola').val($id_escola);

//OBJETO COM TODOS OS CICLOS INATIVOS
let obj = [{
	"name": "Pré Escola",
	"fases": "{\"bercario\":false,\"jardim_de_infancia\":false}"
}, {
	"name": "Ensino Fundamental I",
	"fases": "{\"primeira_serie\":false,\"segunda_serie\":false,\"terceira_serie\":false,\"quarta_serie\":false,\"quinta_serie\":false}"
}, {
	"name": "Ensino Fundamental II",
	"fases": "{\"sexta_serie\":false,\"setima_serie\":false,\"oitava_serie\":false,\"nona_serie\":false}"
}, {
	"name": "Ensino Médio",
	"fases": "{\"primeiro_ano\":false,\"segundo_ano\":false,\"terceiro_ano\":false}"
}];
$all_ciclos = JSON.stringify(obj);

var header = {
	'Accept': 'application/json',
	'REST-range': 'resources=0-1000',
	'Content-Type': 'application/json; charset=utf-8'
};

var insertMasterData = function (ENT, loja, dados, fn) {
	$.ajax({
		url: $site + '/' + loja + '/dataentities/' + ENT + '/documents/',
		type: 'PATCH',
		data: dados,
		headers: header,
		success: function (res) {
			fn(res);
		},
		error: function (res) {
			swal("Oops!", "Houve um problema. Tente novamente mais tarde.", "error");
		}
	});
};

$('.helperComplement ').remove();

function removeLoading() {
	$(document).ajaxStart(function () {
		$('#loading-app').show();
	});

	$(document).ajaxStop(function (event, request, settings) {
		$('#loading-app').hide();

		//FUNÇÕES APÓS REQUISIÇÕES

		//LISTA ALUNO: ADICIONAR OS PRIMEIROS PRODUTOS
		$('.lista-escolar.aluno #add_all').trigger('click');
	});
}
removeLoading();

function getMoney(str) {
	var str = str.toString();
	return parseInt(str.replace(/[\D]+/g, ''));
}

function formatReal(numero) {
	var numero = numero.toFixed(2).split('.');
	numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
	return numero.join(',');
}

function mask() {
	$('#criar_escola input[name="telefone_contato"]').mask('(00) 0000-0000');
}
mask();

function active_pop_up(classe) {
	$('body, #overlay, .popups .pop_up' + classe).addClass('active');
}

function validate() {
	$('#criar_escola input[required="required"]').on('focusout', function () {
		if ($(this).val() == '') {
			$(this).removeClass('success');
			$(this).addClass('alert');
		} else {
			$(this).removeClass('alert');
			$(this).addClass('success');
		}
	});
}
validate();

function logo_tipo(value) {
	//LOGOTIPO - GET LAST
	if (value != null) {
		return value.split(',').pop(-1);
	}
}

function booleano(value) {
	if (typeof value === "boolean") return value;

	if (typeof value === "number") {
		return value === 1 ? true : value === 0 ? false : undefined;
	}

	if (typeof value != "string") return undefined;

	return value.toLowerCase() === 'true' ? true : false;
}

function stringToJson(value) {
	if (typeof value === "string") {
		return JSON.parse(value);
	} else {
		return value;
	}
}

//MODAL PARA IMAGEM
function modal_image() {
	//OPEN
	$(document).on('click', 'img[data-function="modal"]', function () {
		$('#modal_image, #overlay, body').addClass('active');

		let id = $(this).parents('li').attr('data-id');

		$.ajax({
			headers: header,
			type: 'GET',
			url: '/api/catalog_system/pub/products/search?fq=productId:' + id
		}).
		done(function (res, status) {
			console.log(res);

			let product = res[0].items[0];

			//IMAGEM PRINCIPAL
			$('#modal_image figure img').attr('src', product.images[0].imageUrl);

			//ZOOM 
			// $('#modal_image figure').zoom();

			//THUMBS
			$('#modal_image .thumbs ul li').remove();
			$.each(product.images, function (index, item) {
				$('#modal_image .thumbs ul').append('<li><img src="' + item.imageUrl + '" /></li>');
			});

			$('#modal_image .name').text(product.name);
			$('#modal_image .description').text(res[0].metaTagDescription);
			$('#modal_image .price .por').text(formatReal(product.sellers[0].commertialOffer.Price));
			$('#modal_image .price .parc').html('Em até <strong>' + product.sellers[0].commertialOffer.Installments[0].NumberOfInstallments + 'x</strong> ' + ' de <strong>' + formatReal(product.sellers[0].commertialOffer.Installments[0].Value) + '</strong>');

			//ID DO SKU
			$('#modal_image .buy').attr('data-id-sku', product.itemId);
		});
	});

	//TROCA IMAGEM 
	$(document).on('click', '#modal_image .thumbs img', function () {
		$('#modal_image figure img').remove();
		$('#modal_image figure').append('<img src="' + $(this).attr('src') + '" />');

		//ZOOM
		$('#modal_image figure').zoom();
	});

	//CONTADOR
	$('#modal_image .qtd .count').on('click', function (e) {
		e.preventDefault();

		let input = $('#modal_image .qtd input');

		if ($(this).hasClass('count-mais')) {
			var qtd_produtos = parseInt(input.attr('value'));
			qtd_produtos++;
		} else if ($(this).hasClass('count-menos')) {
			var qtd_produtos = parseInt(input.attr('value'));

			if (qtd_produtos > 1) {
				qtd_produtos--;
			}
		}

		input.attr('value', qtd_produtos);
	});

	//BUY
	$('#modal_image .buy').on('click', function (e) {
		e.preventDefault();

		window.open(
			'/checkout/cart/add?sku=' + $(this).attr('data-id-sku') + '&qty=' + $('#modal_image .qtd input').attr('value') + '&seller=1',
			'_blank'
		);
	});

	//CLOSE
	$('#modal_image .close').on('click', function (e) {
		e.preventDefault();

		$('#modal_image, #overlay, body').removeClass('active');

		$('#modal_image img').attr('src', '');
		$('#modal_image .name, #modal_image .price .por').text('');
	});
}
modal_image();

function seta_lateral() {
	$(document).on('click', '.lista-escolar .list > .arrow', function () {
		//TAMANHO DO LI
		let li = $(this).parents('.list').find('li').width() + 30;

		if ($(this).hasClass('prev')) {
			let ul = $(this).next('ul');

			ul.animate({
				scrollLeft: ul.scrollLeft() - li
			});
		} else {
			let ul = $(this).prev('ul');

			ul.animate({
				scrollLeft: ul.scrollLeft() + li
			});
		}
	});
}
seta_lateral();

var edit = (function () {
	var escola = {
		list: function () {
			//EVITA PROBLEMA DE EXIBIR LISTA DE OUTRO CLIENTE
			if ($email_cliente != null) {
				//EVITA REPETIÇÃO
				$('.minhas_escolas li').remove();

				$.ajax({
					url: '/api/dataentities/LE/search?_fields=id_escola,id,ativo,nome_escola,logotipo&first_email=' + $email_cliente,
					type: 'GET',
					headers: header
				}).
				done(function (res) {
					$.each(res, function (a, b) {
						let content = '';
						content += '<li data-id="' + b.id_escola + '" data-document="' + b.id + '" data-ativo="' + b.ativo + '" data-id="' + b.id_escola + '" data-name="' + b.nome_escola + '" data-list="">';
						content += '<div class="column column_1">';
						content += '<img src="http://api.vtex.com/fernet/dataentities/LE/documents/' + b.id + '/logotipo/attachments/' + logo_tipo(b.logotipo) + '" />';
						content += '<p>' + b.nome_escola + '</p>';
						content += '</div>';

						content += '<div class="column column_2">';
						content += '<a href="/lista-escolar/cadastro" class="btn edit escola"><img src="/arquivos/ico-edit.jpg"/> Editar escola</a>';
						content += '<a href="/lista-escolar/cadastro" class="btn edit lista"><img src="/arquivos/ico-edit.jpg"/> Editar lista</a>';
						if (b.ativo === true) {
							content += '<a href="#" class="btn desativar"><span>X</span> Desativar</a>';
						} else {
							content += '<a href="#" class="btn ativar"><span></span> Ativar</a>';
						}
						content += '</div>';
						content += '</li>';

						$('.minhas_escolas').append(content);
					});
				});
			} else {
				// location.reload();
			}
		},

		ativo: function () {
			$(document).on('click', '.content_4 .minhas_escolas li .column_2 .btn.desativar', function (e) {
				e.preventDefault();

				//ID DO DOCUMENTO
				let id = $(this).parents('li').attr('data-document');

				let obj = {
					'ativo': false
				}

				let json = JSON.stringify(obj);

				$.ajax({
					type: 'PATCH',
					url: '/api/dataentities/LE/documents/' + id,
					data: json,
					headers: header
				}).done(function (res, status) {
					//DESTAIVAR ESCOLA
					swal('Escola desativada :(', 'Sua escola será desativada em breve!', 'success');

					escola.list();
				}).fail(function (res, status) {
					swal("Oops!", "Houve um problema. Tente novamente mais tarde.", "error");
				});
			});

			$(document).on('click', '.content_4 .minhas_escolas li .column_2 .btn.ativar', function (e) {
				e.preventDefault();

				//ID DO DOCUMENTO
				let id = $(this).parents('li').attr('data-document');

				let obj = {
					'ativo': true
				}

				let json = JSON.stringify(obj);

				$.ajax({
					type: 'PATCH',
					url: '/api/dataentities/LE/documents/' + id,
					data: json,
					headers: header
				}).done(function (res, status) {
					//DESTAIVAR ESCOLA
					swal('Escola ativa :)', 'Sua escola será ativada em breve!', 'success');

					escola.list();
				}).fail(function (res, status) {
					swal("Oops!", "Houve um problema. Tente novamente mais tarde.", "error");
				});
			});
		}
	};

	//PÁGINA - MINHAS ESCOLAS
	if ($('body').hasClass('lista-escolar cadastro')) {
		escola.list();
		escola.ativo();
	}
})();

var escola = (function () {
	//INICIO
	var content_0 = {
		logado: function (value) {

		},

		create: function () {
			$('.lista-escolar.cadastro .content_0 .create').on('click', function (e) {
				e.preventDefault();

				$('.lista-escolar.cadastro .content_0').fadeOut(300, function () {
					setTimeout(function () {
						$('.lista-escolar.cadastro .content_1').fadeIn(300);
					}, 300);
				});
			});
		},

		view: function () {
			$('.lista-escolar.cadastro .content_0 .view').on('click', function (e) {
				e.preventDefault();

				$('.lista-escolar.cadastro .content_0').fadeOut(300, function () {
					setTimeout(function () {
						$('.lista-escolar.cadastro .content_4').fadeIn(300);
					}, 300);
				});
			});
		}
	};

	// content_0.logado($email_cliente);
	content_0.create();
	content_0.view();

	//INFORMAÇÕES DA ESCOLA
	var content_1 = {
		id: function () {
			let id = Math.floor(Math.random() * 9999999999) + 1;
			$('#criar_escola input[name="id_escola"]').attr('value', id);
		},

		logotipo: function (id) {
			var data = new FormData();

			data.append('logotipo', $('.lista-escolar.cadastro .content_1 #criar_escola .content .step.step_3 .ico_file input')[0].files[0]);

			$.ajax({
				url: 'https://fernet.vtexcommercestable.com.br/api/dataentities/LE/documents/' + id + '/logotipo/attachments',
				type: 'POST',
				data: data,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function () {},
				success: function (data, textStatus, request) {
					console.log(textStatus);
				},
				error: function (data, textStatus, request) {}
			});
		},

		preview: function () {
			function filePreview(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$('.lista-escolar.cadastro .content_5>.head .logo_escola, #criar_escola .ico_file i').html('<img src="' + e.target.result + '" />');
					};
					reader.readAsDataURL(input.files[0]);
				}
			}

			$('#criar_escola input[name="logotipo"]').change(function (event) {
				filePreview(this);
			});
		},

		//UPLOAD DE PDF - OPEN STEP 4
		upload: function () {
			$('#criar_escola .upload .btn').on('click', function (event) {
				event.preventDefault();
				$('#criar_escola .footer.pagination .next').trigger('click');
			});
		},

		pdf: function () {
			let arr = [];
			$('#criar_escola .step_4 .list ul li').each(function (index, item) {
				let serie = $(item).find('select option:selected').val();
				let url = $(item).find('input[name="url"]').val();

				//VALIDAÇÃO
				if (serie != 'Selecione...' && url != '') {
					arr.push({
						'serie': serie,
						'url': url
					});

					let obj = JSON.stringify(arr);
					console.log(obj);

					$('#criar_escola input[name="list_pdf"]').attr('value', obj);
				}
			});
		},

		//PREENCHE O INPUT[NAME="LIST_PDF"]
		list_pdf: function () {
			$(document).on('focusout', '#criar_escola .step_4 .list ul select, #criar_escola .step_4 .list ul input', function () {
				content_1.pdf();
			});
		},

		more: function () {
			$(document).on('click', '#criar_escola .step_4 .list .more', function (event) {
				event.preventDefault();

				let li = $('#criar_escola .step_4 .list li').first().clone();
				$('#criar_escola .step_4 .list ul').append(li);
			});
		},

		create: function () {
			$('#criar_escola').submit(function (event) {
				event.preventDefault();

				//LOADING
				$(this).addClass('loading');

				let list_pdf = $('#criar_escola input[name="list_pdf"]').val().length != 0 ? $('#criar_escola input[name="list_pdf"]').val() : 'false';

				let obj_LE = {
					"ativo": false,
					"id_escola": $('#criar_escola input[name="id_escola"]').val(),
					"nome_escola": $('#criar_escola input[name="nome_escola"]').val(),
					"unidade": $('#criar_escola input[name="unidade"]').val(),
					"nome_responsavel": $('#criar_escola input[name="nome_responsavel"]').val(),
					"cnpj": $('#criar_escola input[name="cnpj"]').val(),
					"ie": $('#criar_escola input[name="ie"]').val(),
					"comissionamento": $('#criar_escola input[name="comissionamento"]').val(),
					"telefone_contato": $('#criar_escola input[name="telefone_contato"]').val(),
					"email": $('#criar_escola input[name="first_email"]').val(), //RELATIONSHIP
					"first_email": $('#criar_escola input[name="first_email"]').val(),
					"second_email": $('#criar_escola input[name="second_email"]').val(),
					"cep": $('#criar_escola input[name="cep"]').val(),
					"endereco": $('#criar_escola input[name="endereco"]').val(),
					"numero": $('#criar_escola input[name="numero"]').val(),
					"complemento": $('#criar_escola input[name="complemento"]').val(),
					"bairro": $('#criar_escola input[name="bairro"]').val(),
					"estado": $('#criar_escola select[name="estado"] option:selected').val(),
					"cidade": $('#criar_escola input[name="cidade"]').val(),
					"descricao": $('#criar_escola textarea[name="descricao"]').val(),
					"list_pdf": list_pdf
				}

				//INFORMAÇÕES DA ESCOLA - ENTIDADE: LE
				let obj = JSON.stringify(obj_LE);

				if ($('#criar_escola').hasClass('editar')) {
					//EDITANDO UMA ESCOLA EXISTENTE
					$.ajax({
						type: 'PATCH',
						headers: header,
						url: '/api/dataentities/LE/documents/' + $('#criar_escola').attr('data-id'),
						data: obj
					}).done(function (res, status) {
						console.log(res);

						//SALVA LOGO NO MASTER DATA
						content_1.logotipo($('#criar_escola').attr('data-id'));

						swal({
								title: 'Escola atualizada!',
								text: 'Sua escola foi atualizada com sucesso.',
								icon: 'success',
								buttons: true,
								dangerMode: true,
							})
							.then((willDelete) => {
								if (willDelete) {
									location.reload();
								}
							});

						//ID DA ESCOLA
						localStorage.setItem('id_escola', $('#criar_escola input[name="id_escola"]').val())
						$('#chave_escola').val($('#criar_escola input[name="id_escola"]').val());
					}).fail(function (res, status) {
						swal("Oops!", "Houve um problema. Tente novamente mais tarde.", "error");
					});
				} else {
					//CRIAR NOVA ESCOLA	
					insertMasterData("LE", 'fernet', obj, function (res) {
						//SALVA LOGO NO MASTER DATA
						content_1.logotipo(res.DocumentId);

						//ID DA ESCOLA
						localStorage.setItem('id_escola', $('#criar_escola input[name="id_escola"]').val())
						$('#chave_escola').val($('#criar_escola input[name="id_escola"]').val());

						//EXIBE OS CICLOS
						$('.content_1').fadeOut(300, function () {
							setTimeout(function () {
								$('.content_5').fadeIn(300);
							}, 300);
						});
					});
				}
			});
		},

		pagination: function () {
			$('#criar_escola .pagination a').on('click', function (e) {
				e.preventDefault();
				if ($(this).hasClass('prev')) {
					var i = parseInt($('#criar_escola .pagination').attr('data-step')) - 1;

					if ($('#criar_escola .pagination').attr('data-step') === '1') {} else {
						$('#criar_escola .pagination, #criar_escola .status').attr('data-step', i);

						$('#criar_escola .step').fadeOut(300, function () {
							setTimeout(function () {
								$('#criar_escola .step_' + i).fadeIn(300);
							}, 300);
						});
					}
				} else if ($(this).hasClass('next')) {
					var i = parseInt($('#criar_escola .pagination').attr('data-step')) + 1;

					if (i < 5) {
						$('#criar_escola .pagination, #criar_escola .status').attr('data-step', i);

						$('#criar_escola .step').fadeOut(300, function () {
							setTimeout(function () {
								$('#criar_escola .step_' + i).fadeIn(300);
							}, 300);
						});
					}
				}
			});

			//CONFIRMAÇÃO DE CADASTRO
			$('#criar_escola .step_4 .back').on('click', function (e) {
				e.preventDefault();

				//100%
				$('#criar_escola .status').attr('data-step', 4);

				$('#criar_escola .pagination .prev').trigger('click');
				$('#criar_escola .pagination').show();
			});
		},

		search_cep: function () {
			$('#criar_escola input[name="cep"]').on('focusout', function () {
				var cep = $('#criar_escola input[name="cep"]').val();
				cep = cep.replace('-', '');

				$.ajax({
					url: 'https://viacep.com.br/ws/' + cep + '/json/',
					type: 'GET',
					success: function (res) {
						if (res.erro) {
							swal("Oops", "CEP inválido!", "warning");
						} else {
							if (res.logradouro) {
								$('#criar_escola input[name="endereco"]').val(res.logradouro);
								$('#criar_escola input[name="endereco"]').addClass('success');
							}

							if (res.complemento) {
								$('#criar_escola input[name="complemento"]').val(res.complemento);
								$('#criar_escola input[name="complemento"]').addClass('success');
							}

							if (res.bairro) {
								$('#criar_escola input[name="bairro"]').val(res.bairro);
								$('#criar_escola input[name="bairro"]').addClass('success');
							}

							if (res.localidade) {
								$('#criar_escola input[name="cidade"]').val(res.localidade);
								$('#criar_escola input[name="cidade"]').addClass('success');
							}

							if (res.uf) {
								$('#criar_escola select[name="estado"] option[value="' + res.uf + '"]').attr('selected', 'selected');
								$('#criar_escola select[name="estado"]').addClass('success');
							}
						}
					},
					error: function (res) {
						swal("Oops", "Houve um erro. Tente mais tarde!", "error");
					}
				});
			});
		},
	};

	content_1.id();
	content_1.preview();
	content_1.create();
	content_1.upload();
	content_1.list_pdf();
	content_1.more();
	content_1.pagination();
	content_1.search_cep();

	//CICLOS
	var content_2 = {
		checked: function () {
			$(document).on('click', 'span[data-function="checkbox"], a[data-function="checkbox"]', function (e) {
				e.preventDefault();

				$(this).toggleClass('checked');
			});
		},

		cadastrar_material: function () {
			$('.content_2 .cadastrar_material').on('click', function (e) {
				e.preventDefault();

				$('.content_2').fadeOut(300, function () {
					setTimeout(function () {
						$('.content_5').fadeIn(300);
					}, 300);
				});
			});
		}
	};

	//content_2.checked();
	content_2.cadastrar_material();

	//MINHAS ESCOLA
	var content_4 = {
		//BUSCA CICLOS DA ESCOLA SELECIONADA
		search_ciclos: function (id) {
			console.log(id);

			$.ajax({
				type: 'GET',
				headers: header,
				url: '/api/dataentities/LC/search?_fields=name,id,ativo,products,ciclos,qtd_recomendada,message&id_escola=' + id
			}).
			done(function (res) {
				$.each(res, function (a, b) {
					console.log(b);

					$('.lista-escolar.cadastro .content_5>.content .nv_1[data-name="' + b.name + '"]').addClass('new');

					$('.lista-escolar.cadastro .content_5>.content .nv_1[data-name="' + b.name + '"] input[data-name="document"]').attr('value', b.id);

					//ATIVO?
					$('.lista-escolar.cadastro .content_5>.content .nv_1[data-name="' + b.name + '"]').attr('data-ativo', b.ativo);
					if (b.ativo === true) {
						$('.lista-escolar.cadastro .content_5>.content .nv_1[data-name="' + b.name + '"] .head span[data-function="checkbox"]').trigger('click');
					}

					//PRODUTOS
					$('.lista-escolar.cadastro .content_5>.content .nv_1[data-name="' + b.name + '"]').attr('data-list', b.products);

					//CHECK OS PRODUTOS ATIVOS
					if (b.products === '' || b.products === null) {} else {
						let list = b.products.split(',');

						$.each(list, function (key, value) {
							$('.lista-escolar.cadastro .content_5>.content .nv_1[data-name="' + b.name + '"] li.nv_2[data-id="' + value + '"] span[data-function="list"]').trigger('click');
						});
					}

					//CICLOS
					$('.lista-escolar.cadastro .content_5>.content .nv_1[data-name="' + b.name + '"] input[data-name="ciclos"]').attr('value', b.ciclos);

					//QUANTIDADE RECOMENDADA
					$('.lista-escolar.cadastro .content_5>.content .nv_1[data-name="' + b.name + '"]').attr('data-quantidade', b.qtd_recomendada);
					
					//MENSAGEM
					$('.lista-escolar.cadastro .content_5>.content .nv_1[data-name="' + b.name + '"] input[data-name="message"]').attr('value', b.message);
				});
			});
		},

		edit: function () {
			//EDITAR ESCOLA
			$(document).on('click', '.content_4 .minhas_escolas li .column_2 .btn.edit.escola', function (e) {
				e.preventDefault();

				let id = $(this).parents('li').data('id');

				//CLASS NO FORMULARIO
				$('#criar_escola').addClass('editar');

				//ID DA ESCOLA
				localStorage.setItem('id_escola', id);
				$('#chave_escola').val(id);

				$.ajax({
					type: 'GET',
					headers: header,
					url: '/api/dataentities/LE/search?_fields=id,id_escola,nome_escola,unidade,nome_responsavel,cnpj,ie,comissionamento,telefone_contato,second_email,cep,endereco,numero,complemento,bairro,estado,cidade,logotipo,descricao,list_pdf&id_escola=' + id
				}).
				done(function (res) {
					console.log(res);

					//ID DO DOCUMENTO
					$('#criar_escola').attr('data-id', res[0].id);

					//PREENCHE FORMULÁRIO DA ESCOLA
					$('#criar_escola input[name="id_escola"]').val(res[0].id_escola);
					$('#criar_escola input[name="nome_escola"]').val(res[0].nome_escola);
					$('#criar_escola input[name="unidade"]').val(res[0].unidade);
					$('#criar_escola input[name="nome_responsavel"]').val(res[0].nome_responsavel);
					$('#criar_escola input[name="cnpj"]').val(res[0].cnpj);
					$('#criar_escola input[name="ie"]').val(res[0].ie);
					$('#criar_escola input[name="comissionamento"]').val(res[0].comissionamento);

					//CONTATO
					$('#criar_escola input[name="telefone_contato"]').val(res[0].telefone_contato);
					$('#criar_escola input[name="second_email"]').val(res[0].second_email);

					//ENDEREÇO
					$('#criar_escola input[name="cep"]').val(res[0].cep);
					$('#criar_escola input[name="endereco"]').val(res[0].endereco);
					$('#criar_escola input[name="numero"]').val(res[0].numero);
					$('#criar_escola input[name="complemento"]').val(res[0].complemento);
					$('#criar_escola input[name="bairro"]').val(res[0].bairro);
					$('#criar_escola select[name="estado"] option[value="' + res[0].estado + '"]').attr('selected', 'selected');
					$('#criar_escola input[name="cidade"]').val(res[0].cidade);

					//LOGO E DESCRIÇÃO
					$('#criar_escola .ico_file i svg').remove();
					$('#criar_escola .ico_file i').append('<img src="http://api.vtex.com/fernet/dataentities/LE/documents/' + res[0].id + '/logotipo/attachments/' + logo_tipo(res[0].logotipo) + '" />');
					$('#criar_escola textarea[name="descricao"]').text(res[0].descricao);

					//LISTA DE PDFS
					if (res[0].list_pdf != 'false') {
						let list = JSON.parse(res[0].list_pdf);

						$('#criar_escola .step_4 .list ul li:not(.clone)').remove();

						$(list).each(function (index, item) {
							$('#criar_escola .step_4 .list ul').append('<li data-index="' + index + '"><span><select data-checked=""><!-- PRÉ ESCOLA --><option value="bercario">Berçario</option><option value="jardmin_de_infancia">Jardmin de Infância</option><!-- ENSINO FUNDAMENTAL I --><option value="primeira_serie">1ª Série</option><option value="segunda_serie">2ª Série</option><option value="terceira_serie">3ª Série</option><option value="quarta_serie">4ª Série</option><option value="quinta_serie">5ª Série</option><!-- ENSINO FUNDAMENTAL II --><option value="sexta_serie">6ª Série</option><option value="setima_serie">7ª Série</option><option value="oitava_serie">8ª Série</option><option value="nona_serie">9ª Série</option><!-- ENSINO MÉDIO --><option value="primeiro_ano">1° Ano</option><option value="segundo_ano">2° Ano</option><option value="terceiro_ano">3° Ano</option></select></span><span><input type="text" name="url" placeholder="Link do PDF..."></span><span><a href="#" class="more">+</a></span></li>');
						});

						$(list).each(function (index, item) {
							$('#criar_escola .step_4 .list ul li[data-index="' + index + '"] input[name="url"]').val(item.url);
							$('#criar_escola .step_4 .list ul li[data-index="' + index + '"] select option[value="' + item.serie + '"]').attr('selected', 'selected');
						});
					}

					//EXIBE FORMULÁRIO DA ESCOLA
					$('.lista-escolar.cadastro .content_4').fadeOut(300, function () {
						setTimeout(function () {
							$('.lista-escolar.cadastro .content_1').fadeIn(300);
						}, 300);
					});
				});
			});

			//EDITAR LISTA
			$(document).on('click', '.content_4 .minhas_escolas li .column_2 .btn.edit.lista', function (e) {
				e.preventDefault();

				let id = $(this).parents('li').data('id');

				//ID DA ESCOLA
				localStorage.setItem('id_escola', id);
				$('#chave_escola').val(id);

				$('.lista-escolar.cadastro .content_4').fadeOut(300, function () {
					setTimeout(function () {
						$('.lista-escolar.cadastro .content_5').fadeIn(300);
					}, 300);
				});

				content_4.search_ciclos($(this).parents('li').data('id'));
			});
		}
	};

	content_4.edit();

	//CADASTRAR MATERIAL - LISTA DE MATERIAIS
	var content_5 = {
		search_categoria: function () {
			$('#search_categoria input[type="text"]').on('keyup', function () {
				let string = $(this).val();

				$('.lista-escolar.cadastro .content_5>.content>ul>li.nv_1').css('display', 'none');
				$('.lista-escolar.cadastro .content_5>.content>ul>li.nv_1 .head .column_1 p').each(function (index, el) {
					let thisText = $(el).text();
					thisText = thisText.toLowerCase();
					string = string.toLowerCase();

					if (thisText.indexOf(string) != -1) {
						$(el).parents('.nv_1').css('display', 'block');
					}
				});
			});
		},

		format_price: function (price) {
			price = price.toString().replace('.', ',');
			price = 'R$ ' + price;

			return price;
		},

		//PASSE O NOME DA CATEGORIA
		get_list_checked: function (nome) {
			let list = [];

			$('.cadastrar-lista-material .content_5>.content>ul>li[data-name="' + nome + '"] .list ul li').each(function (a, b) {
				let id = $(b).find('.checked').data('id');

				if (id != undefined) {
					list.push(id);
				}
			});

			//SOMA
			// let atual = $('.cadastrar-lista-material .content_5>.content>ul>li[data-name="' + nome + '"]').attr('data-list');
			$('.cadastrar-lista-material .content_5>.content>ul>li[data-name="' + nome + '"]').attr('data-list', list);
		},

		//TRAZ OS PRODUTOS - CATEGORIAS
		get_categorias: function () {
			$.ajax({
				url: 'https://dk10.com.br/fernet/add_site/connect-api.php',
				dataType: 'json',
				method: 'GET',
				action: 'GET',
				data: {
					parametro: $site + '/api/catalog_system/pub/category/tree/3/'
				},
				success: function (response) {
					$(response).each(function (a, b) {
						if (b.name === 'Papelaria') {
							$(b.children).each(function (x, y) {
								let children = y.children;

								$(children).each(function (e, d) {
									let content = '';

									content += '<li class="nv_1" data-ativo="false" data-categoria="' + y.id + '" data-id="' + d.id + '" data-name="' + d.name + '" data-ordenacao="" data-list="" data-quantidade="" data-pdf="">';

									//DOCUMENT ID - CASO DE EDIÇÃO
									content += '<input type="text" class="dis-none" data-name="document" value="" />';

									//CICLOS ATIVOS
									content += "<input type='text' class='dis-none' data-name='ciclos' value='" + $all_ciclos + "' />";

									//MENSAGEM AOS ALUNOS
									content += '<input type="text" class="dis-none" data-name="message" />'

									//HEAD
									content += '<div class="head">';
									//NOME
									content += '<div class="column column_1">';
									content += '<span data-function="checkbox"></span>';
									content += '<p>' + d.name + '</p>';
									content += '</div>';

									//EXIBIR
									content += '<div class="column column_2">';
									content += '<p class="dis-none">Exibir:</p>';
									content += '<ol></ol>';
									content += '</div>';

									//CICLO
									content += '<div class="column column_3">';
									content += '<p class="dis-none">Ciclo:</p>';
									content += '<ol></ol>';
									content += '</div>';

									//EDITAR
									content += '<div class="column column_4">';
									content += '<a href="#" class="edit">Editar</a>';
									content += '<a href="#" class="exibir">Exibir</a>';
									content += '</div>';
									content += '</div>';
									//FIM - HEAD

									//LIST
									content += '<div class="list slide">';
									content += '<button class="arrow prev"></button>';
									content += '<ul></ul>';
									content += '<button class="arrow next"></button>';
									content += '<a href="#" class="add_all">Adicionar todos</a>';
									content += '</div>';
									content += '</li>';

									//CADASTRAR MATERIAL - LISTA DE MATERIAIS
									if (d.name === 'Livro de Atividades' || d.name === 'Livro de Colorir' || d.name === 'Livro de Historias' || d.name === 'Livro Ata' || d.name === 'Livro de Registro') {
										//DEPARTAMENTO DE LIVROS
										$('.cadastrar-lista-material .content_5 .content > ul.list_livros').append(content);
									} else {
										$('.cadastrar-lista-material .content_5 .content > ul:not(.list_livros)').append(content);
									}
								});
							});
						}
					});

					//PRIMEIRA CATEGORIA ABERTA
					$('.cadastrar-lista-material .content_5 .content > ul li:first-child .exibir').trigger('click');

					$('.cadastrar-lista-material .content_5 .content > ul li').each(function (a, b) {
						let id = $(b).data('id');
						let list = $(b).find('.list ul');
						let slide = $(b).find('.list.slide');
						let categoria = $(b).data('categoria');

						$.ajax({
							url: 'https://dk10.com.br/fernet/add_site/connect-api.php',
							dataType: 'json',
							method: 'GET',
							action: 'GET',
							data: {
								parametro: $site + '/api/catalog_system/pub/products/search?fq=C:8/' + categoria + '/' + id
							},
							success: function (response) {
								if (response.length) {
									//HÁ PRODUTOS NA CATEGORIA
									$(response).each(function (index, d) {
										let imageUrl = d.items[0].images ? d.items[0].images[0].imageUrl : '/arquivos/image-not-found.jpg';
										let de = d.items[0].sellers[0].commertialOffer.ListPrice;
										let por = d.items[0].sellers[0].commertialOffer.Price;

										//APENAS PRODUTOS COM ESTOQUE
										if (d.items[0].sellers[0].commertialOffer.Price != 0) {
											//QUANTIDADE DE PRODUTOS
											slide.attr('data-quantidade', index + 1);

											let content = '';
											content += '<li class="nv_2" data-id="' + d.productId + '">';
											content += '<span data-function="list" data-id="' + d.productId + '"></span>';
											content += '<figure><img src="' + imageUrl + '" data-function="modal" /></figure>';
											content += '<p class="name">' + d.productName + '</p>';

											content += '<div class="price">';

											if (d.items[0].sellers[0].commertialOffer.ListPrice != d.items[0].sellers[0].commertialOffer.Price) {
												content += '<p class="de">' + content_5.format_price(de) + '</p>';
											}

											content += '<p class="por">' + content_5.format_price(por) + '</p>';
											content += '</div>';
											content += '</li>';

											list.append(content);
										}
									});
								} else {
									//NÃO HÁ PRODUTOS NA CATEGORIA
									slide.attr('data-quantidade', 0);
									list.append('<p class="no-estoque">Não há produtos nesta categoria.</p>');

									//SEM ESTOQUE
									$(b).attr('data-available="false"');
								}
							}
						});
					});
				}
			});
		},

		//EXIBE MAIS OPCOES
		more: function () {
			$('.cadastrar-lista-material .content_5>.content>.more').on('click', function (event) {
				event.preventDefault();
				$(this).siblings('ul').addClass('active');
				$(this).remove();
			});
		},

		//SALVA OS ITENS SELECIONADOS EM DATA-LIST
		data_list: function () {
			$(document).on('click', '.content_5 .list span[data-function="list"]', function (e) {
				e.preventDefault();

				$(this).toggleClass('checked');

				let nome_categoria = $(this).parents('.nv_1').data('name');

				content_5.get_list_checked(nome_categoria);
			});
		},

		//SALVA TODOS OS ITENS EM DATA-LIST
		add_all: function () {
			$(document).on('click', '.lista-escolar.cadastro .content_5>.content>ul>li.nv_1 .add_all', function (e) {
				e.preventDefault();

				$(this).parents('.list').find('span[data-function="list"]:not(.checked)').trigger('click');
			});
		},

		//ATIVA O CICLO
		ativa: function () {
			$(document).on('click', '.cadastrar-lista-material .nv_1 .head span[data-function=checkbox]', function () {
				$(this).toggleClass('checked');

				if ($(this).hasClass('checked')) {
					$(this).parents('.nv_1').attr('data-ativo', true);
				} else {
					$(this).parents('.nv_1').attr('data-ativo', false);
				}
			});
		},

		//ORDENAÇÃO
		ordenar: function () {
			$('.pop_up.editar .ordenar a').on('click', function (e) {
				e.preventDefault();

				$('.pop_up.editar .ordenar a').removeClass('active');
				$(this).addClass('active');

				$('.lista-escolar.cadastro .content_5>.content>ul>li.editando').attr('data-ordenacao', $(this).attr('data-name'));
			});
		},

		add_product: function () {
			$('#add_product').submit(function (event) {
				event.preventDefault();

				let novo = $('#add_product input[type="tel"]').val();
				let atual = $('.lista-escolar.cadastro .content_5>.content>ul>li.editando').attr('data-list');

				$.ajax({
					url: '/api/catalog_system/pub/products/search?fq=productId:' + novo,
					type: 'GET',
					headers: header
				}).
				done(function (response, status) {
					console.log(response);

					if (response.length === 0) {
						swal('Oops!', 'Produto não encontrado', 'warning');
					} else {
						swal('Produto adicionado!', 'O produto foi adicionado a lista', 'success');
						

						let imageUrl = response[0].items[0].images ? response[0].items[0].images[0].imageUrl : '/arquivos/image-not-found.jpg';
						let de = response[0].items[0].sellers[0].commertialOffer.ListPrice;
						let por = response[0].items[0].sellers[0].commertialOffer.Price;

						//APENAS PRODUTOS COM ESTOQUE
						if (response[0].items[0].sellers[0].commertialOffer.Price != 0) {
							let content = '';
							content += '<li class="nv_2" data-id="' + response[0].productId + '">';
							content += '<span data-function="list" data-id="' + response[0].productId + '"></span>';
							content += '<figure><img src="' + imageUrl + '" data-function="modal" /></figure>';
							content += '<p class="name">' + response[0].productName + '</p>';

							content += '<div class="price">';

							if (response[0].items[0].sellers[0].commertialOffer.ListPrice != response[0].items[0].sellers[0].commertialOffer.Price) {
								content += '<p class="de">' + content_5.format_price(de) + '</p>';
							}

							content += '<p class="por">' + content_5.format_price(por) + '</p>';
							content += '</div>';
							content += '</li>';

							//ADD A LISTA
							$('.lista-escolar.cadastro .content_5>.content>ul>li.editando .list ul').append(content);
							$('.lista-escolar.cadastro .content_5>.content>ul>li.editando .list ul li[data-id="' + novo + '"] span[data-function="list"]').trigger('click');
						} else {
							swal('Oops!', 'Produto sem estoque', 'warning');
						}
					}

					//RESETA O FORM
					$('#add_product').trigger('reset');
				});
			});
		},

		//QUANTIDADE RECOMENDADA
		qtd_recomendada: function () {
			$('#qtd_recomendada').keyup(function () {
				$('.lista-escolar.cadastro .content_5>.content>ul>li.editando').attr('data-quantidade', $(this).val());
			});
		},

		//MENSAGEM AO ALUNO
		message: function () {
			$('#message').keyup(function () {
				$('.lista-escolar.cadastro .content_5>.content>ul>li.editando input[data-name="message"]').attr('value', $(this).val());
			});
		},

		//LINK DO PDF
		link_pdf: function () {
			$('#link_pdf').keyup(function () {
				$('.lista-escolar.cadastro .content_5>.content>ul>li.editando').attr('data-pdf', $(this).val());
			});
		},

		//PERIODO
		periodo: function () {
			$('#periodo').on('click', function (e) {
				e.preventDefault();
				$(this).toggleClass('checked');
			});
		},

		//EXIBIR LISTA DE PRODUTOS OCULTO
		exibir: function () {
			$(document).on('click', 'main .content_5 .head a.exibir', function (e) {
				e.preventDefault();

				$(this).toggleClass('active');
				$(this).parents('li').find('.list').slideToggle();
			});
		},

		//CONSTROI JSON DE ENTIDADES
		cadastrar_ciclos: function () {			
			$(document).on('click', '.popups .pop_up.editar .cadastrar_ciclos a[data-function="checkbox"]', function (e) {
				e.preventDefault();

				if ($(this).hasClass('btn integral')) {
					$(this).toggleClass('checked');
					$(this).prev('a').toggleClass('integral');
				} else {
					$(this).toggleClass('checked');
					$(this).removeClass('integral');
					$(this).next('a').removeClass('checked');	
				}

				function build(entidade, list) {
					$('.popups .pop_up.editar .cadastrar_ciclos .columns ul[data-entidade="' + entidade + '"] li a:not(.btn)').each(function (index, item) {
						let name = '';
						let value = $(item).hasClass('checked') ? true : false;
					
						if ($(item).hasClass('checked integral')) {
							name = $(item).attr('data-name') + '_integral';
						} else {
							name = $(item).attr('data-name');    
						}
					
						list[name] = value;
					});

					return list;
				};

				let arr = [];

				arr.push({
					'name': 'Pré Escola',
					'fases': build('PE', {})
				});

				arr.push({
					'name': 'Ensino Fundamental I',
					'fases': build('EF', {})
				});

				arr.push({
					'name': 'Ensino Fundamental II',
					'fases': build('ED', {})
				});

				arr.push({
					'name': 'Ensino Médio',
					'fases': build('EM', {})
				});

				let ciclos = JSON.stringify(arr);

				//CATEGORIA QUE ESTÁ SENDO EDITADA
				$('.cadastrar-lista-material .content_5 li.editando input[data-name="ciclos"]')
					.attr('value', ciclos);
			});
		},

		//CONSTROI OBJETO DE PRODUTOS SELECIONADOS
		build_products: function () {
			let arr = [];

			$('.cadastrar-lista-material .content_5>.content li.nv_1').each(function (a, b) {
				arr.push({
					'name': $(b).data('name'),
					'list': $(b).attr('data-list')
				});
			});

			return arr;
		},

		//CONCLUI O CADASTRO
		concluido: function () {
			$('.cadastrar-lista-material .content_5 .concluido').on('click', function (e) {
				e.preventDefault();

				$(this).addClass('disabled');

				//CONTAGEM DE REQUISIÇÃO
				let count = $('.cadastrar-lista-material .content_5>.content .nv_1[data-ativo="true"]').length;

				//CADASTRAR OS CICLOS
				$('.cadastrar-lista-material .content_5>.content .nv_1').each(function (index, item) {

					//DELETA O CICLO INATIVO
					if ($(this).hasClass('new')) {
						let id = $(this).find('input[data-name="document"]').val();

						var data = "";

						var xhr = new XMLHttpRequest();
						xhr.withCredentials = true;

						xhr.addEventListener("readystatechange", function () {
							if (this.readyState === 4) {
								console.log(this.responseText);
							}
						});

						xhr.open("DELETE", "/api/dataentities/LC/documents/" + id);
						xhr.setRequestHeader("Content-Type", "application/json");
						xhr.setRequestHeader("Accept", "application/vnd.vtex.ds.v10+json");
						xhr.setRequestHeader("x-vtex-api-appKey", "vtexappkey-fernet-NYTWTR");
						xhr.setRequestHeader("x-vtex-api-appToken", "LNJQFEAMQWZRANNHNQKJPRDZALSIQMPYWAJPIBWSARPHOBMWJDXWCRCTJJPTLRWEGAUADYYLURJXBWKZZVZMOTVFXPGZLCGIVKBOGLTKFLCKIESMYQUQSJQMEPDOSZAR");

						xhr.send(data);
					}

					let ativo = booleano($(this).attr('data-ativo'));
					let ciclos = $(this).find('input[data-name="ciclos"]').val();
					let message = $(this).find('input[data-name="message"]').val();
					let name = $(this).attr('data-name');
					let products = $(this).attr('data-list');
					let qtd_recomendada = $(this).attr('data-quantidade');
					let link_pdf = $(this).attr('data-pdf');

					//SALVA APENAS OS CICLOS ATIVOS
					if (ativo === true) {
						console.log('____');
						console.log(ativo);
						console.log(ciclos);
						console.log(name);
						console.log(products);
						console.log(qtd_recomendada);
						console.log(message);
						console.log(link_pdf);
						console.log($email_cliente);
						console.log($id_escola);
						console.log('____');

						let dados = {
							'ativo': ativo,
							'ciclos': ciclos,
							'message': message,
							'email': $email_cliente,
							'id_escola': $('#chave_escola').val(),
							'name': name,
							'products': products,
							'qtd_recomendada': qtd_recomendada,
							'link_pdf': link_pdf
						}

						let obj = JSON.stringify(dados);

						insertMasterData('LC', 'fernet', obj, function (res) {
							console.log(res);

							if (index += 1 === count) {
								$('.cadastrar-lista-material .pop_up.lista, #overlay').addClass('active');
								$('.cadastrar-lista-material .content_5>.footer a').removeClass('disabled');
							}
						});
					}
				});
			});
		},

		//ORDENAÇÃO
		ordenacao: function () {}
	};

	if ($('body').hasClass('lista-escolar cadastro')) {
		content_5.search_categoria();
		content_5.get_categorias();
		content_5.data_list();
		content_5.more();
		content_5.add_all();
		content_5.ativa();
		content_5.ordenar();
		content_5.add_product();
		content_5.qtd_recomendada();
		content_5.message();
		content_5.link_pdf();
		content_5.periodo();
		content_5.exibir();
		content_5.cadastrar_ciclos();
		content_5.concluido();
	}

	//POP UPS 
	var popups = {
		aviso: function () {
			$('body.lista-escolar header a, body.lista-escolar footer a').on('click', function (e) {
				e.preventDefault();

				let url = $(this).attr('href');

				swal({
						title: 'Você tem certeza?',
						text: 'As informações não serão gravados após a saída.',
						icon: 'warning',
						buttons: true,
						dangerMode: true,
					})
					.then((willDelete) => {
						if (willDelete) {
							window.location = url;
						}
					});
			});
		},

		editar: function () {
			//OPEN
			$(document).on('click', '.cadastrar-lista-material .content_5 a.edit', function (e) {
				e.preventDefault();

				//LI QUE ESTÁ SENDO EDITADA
				$(this).parents('.nv_1').addClass('editando');

				$('.popups .pop_up.editar').addClass('active');

				//ATUALIZA NOME DA CATEGORIA NO POP UP
				let name = $(this).parents('li').data('name');
				$('.popups .pop_up.editar .head .name').text(name);

				//ATUALIZA OS CICLOS DE ACORDO COM DATA-CICLOS DO LI.EDITANDO
				let arr = $(this).parents('.nv_1').find('input[data-name="ciclos"]').val();
				let ciclos = JSON.parse(arr);

				$(ciclos).each(function (a, b) {

					let fases = stringToJson(b.fases);

					$.each(fases, function (key, value) {
						if (value === true) {

							let integral = key;
							let common = integral.split('_integral')[0];
							
							if (key.includes('_integral')) {
								$('.popups .pop_up.editar .cadastrar_ciclos ul li a[data-name="' + integral + '"]').addClass('checked');
								$('.popups .pop_up.editar .cadastrar_ciclos ul li a[data-name="' + common + '"]').addClass('checked integral');
							} else {
								$('.popups .pop_up.editar .cadastrar_ciclos ul li a[data-name="' + common + '"]').addClass('checked');
							}
						} else {
							$('.popups .pop_up.editar .cadastrar_ciclos ul li a[data-name="' + key + '"]').removeClass('checked');
						}
					});
				});

				//ATUALIZA A ORDENAÇÃO
				$('.popups .pop_up.editar .ordenar a').removeClass('active');
				$('.popups .pop_up.editar .ordenar a[data-name="' + $(this).parents('.nv_1').attr('data-ordenacao') + '"]').addClass('active');

				//ATUALIZA A QUANTIDADE RECOMENDADA
				$('.popups .pop_up.editar #qtd_recomendada').val($(this).parents('.nv_1').attr('data-quantidade'));

				//ATUALIZA MENSAGEM
				let message = $(this).parents('.nv_1').find('input[data-name="message"]').val();
				$('.popups .pop_up.editar #message').text(message);

				//ATUALIZA LINK DO PDF
				$('.popups .pop_up.editar #link_pdf').val($(this).parents('.nv_1').attr('data-pdf'));
			});

			//CLOSE
			$(document).on('click', '.popups .pop_up.editar .close, .popups .pop_up.editar .footer a', function (e) {
				e.preventDefault();
				$('.popups .pop_up.editar').removeClass('active');

				$('.cadastrar-lista-material .content_5>.content>ul>li').removeClass('editando');
			});
		}
	};

	popups.aviso();

	if ($('body').hasClass('lista-escolar cadastro')) {
		popups.editar();
	}
})();

var aluno = (function () {
	var step_1 = {
		busca_escola: function () {
			$.ajax({
				type: 'GET',
				headers: header,
				url: '/api/dataentities/LE/search?_fields=ativo,id_escola,id,logotipo,nome_escola,unidade,cidade,estado'
			}).
			done(function (res) {
				//HÁ RESULTADO
				if (res.length != 0) {
					//LISTA AS ESCOLAS ATIVAS
					$(res).each(function (index, b) {
						console.log(b);
						if (b.ativo === true) {
							let content = '';
							content += '<li class="dis-none" data-id="' + b.id_escola + '" data-cidade="' + b.cidade + '" data-estado="' + b.estado + '">';
							content += '<figure>';
							content += '<img src="http://api.vtex.com/fernet/dataentities/LE/documents/' + b.id + '/logotipo/attachments/' + logo_tipo(b.logotipo) + '" alt="' + b.nome_escola + '" />';
							content += '</figure>';
							content += '<div class="text">';
							content += '<p><strong>Nome: </strong>' + b.nome_escola + '</p>';
							if (b.unidade) {
								content += '<p><strong>Unidade: </strong>' + b.unidade + '</p>';
							}
							content += '</div>';
							content += '</li>';

							$('section[data-step="1"] > ul').append(content);
						}
					});
				}
			});

			//SELECT
			$('#busca_escola select').change(function () {
				let text = $('#busca_escola select option:selected').text();
				$('#busca_escola input[type="text"]').attr('placeholder', 'Procure por: ' + text);
			});

			//BUSCAR ESCOLA
			$("#busca_escola").submit(function (event) {
				event.preventDefault();

				if ($('#busca_escola input[type="text"]').val() === '') {
					$('.lista-escolar.aluno main section[data-step="1"]>ul li').removeClass('dis-none');
				} else {
					let string = $('#busca_escola input[type="text"]')
						.val()
						.toLowerCase()
						.replace(/[áàâã]/, 'a')
						.replace(/[éèê]/, 'e')
						.replace(/[óòôõ]/, 'o')
						.replace(/[úùû]/, 'u');

					console.log(string);

					let type = $('.lista-escolar.aluno main form select option:selected').val();

					$('.lista-escolar.aluno main section[data-step="1"]>ul li').addClass('dis-none');
					$('.lista-escolar.aluno main section[data-step="1"]>ul li').each(function (index, el) {
						if (type === 'nome_escola') {
							var thisText = $(el).find('p')
								.text()
								.toLowerCase()
								.replace(/[áàâã]/, 'a')
								.replace(/[éèê]/, 'e')
								.replace(/[óòôõ]/, 'o')
								.replace(/[úùû]/, 'u');
						} else {
							var thisText = $(el).data(type)
								.toLowerCase()
								.replace(/[áàâã]/, 'a')
								.replace(/[éèê]/, 'e')
								.replace(/[óòôõ]/, 'o')
								.replace(/[úùû]/, 'u');
						}

						if (thisText.indexOf(string) != -1) {
							$(el).removeClass('dis-none');
						}
					});
				}
			});
		},

		//AO SELECIONAR A ESCOLA - MUDA PARA 2.CICLO E TRAZ INFORMACOES DA ESCOLA
		busca_info: function () {
			$(document).on('click', '.lista-escolar.aluno section[data-step="1"]>ul li', function () {
				let id = $(this).data('id').toString();

				$(this).siblings('li').removeClass('active');
				$(this).addClass('active');

				//INFORMAÇÕES DA ESCOLA
				$.ajax({
					type: 'GET',
					headers: header,
					url: '/api/dataentities/LE/search?_fields=id_escola,id,nome_escola,unidade,descricao,logotipo&id_escola=' + id
				}).
				done(function (res) {
					console.log(res);

					$(res).each(function (a, b) {
						if (b.id_escola === id) {
							let nome = b.unidade != null ? b.nome_escola + ' - ' + b.unidade : b.nome_escola;
							$('.lista-escolar.aluno section[data-step="2"] figure img').attr('src', 'http://api.vtex.com/fernet/dataentities/LE/documents/' + b.id + '/logotipo/attachments/' + logo_tipo(b.logotipo));
							$('.lista-escolar.aluno section[data-step="2"] .nome_escola').text(nome);
							$('.lista-escolar.aluno section[data-step="2"] .descricao_escola').text(b.descricao);
						}
					});
				});

				//BUSCA INFORMAÇÕES DO CICLO
				functions.busca_info_ciclo(id);
			});
		}
	};

	var step_2 = {
		busca_serie: function () {
			//BUSCA AS SÉRIES
			$('.lista-escolar.aluno main section[data-step="2"] article ul li').on('click', function () {
				$(this).siblings('li').removeClass('active');
				$(this).addClass('active');

				//ATIVA 3. SÉRIE
				$('.lista-escolar.aluno section[data-step="2"]').fadeOut(300, function () {
					setTimeout(function () {
						$('.lista-escolar.aluno section[data-step="3"]').fadeIn(300);
					}, 300);
				});

				//SÉRIE
				let serie = $(this).data('name');

				//ID DA ESCOLA
				let id = $('.lista-escolar.aluno main section[data-step="1"]>ul li.active').data('id').toString();

				//NOME DO CICLO
				$('.lista-escolar.aluno main section[data-step="3"] .nome_ciclo').text(serie);

				//LISTA AS SÉRIES
				$.ajax({
					type: 'GET',
					headers: header,
					url: '/api/dataentities/LC/search?_fields=ciclos&id_escola=' + id
				}).
				done(function (res) {
					//OCULTA TODOS
					$('section[data-step="3"] .list li').addClass('dis-none');

					$(res).each(function (a, b) {
						let ciclos = stringToJson(b.ciclos);

						$(ciclos).each(function (c, d) {
							if (d.name === serie) {

								let fases = stringToJson(d.fases);

								$.each(fases, function (key, value) {
									if (value === true) {
										$('section[data-step="3"] .list li[data-name="' + key + '"]').removeClass('dis-none');											
									}
								});
							}
						});
					});
				});
			});
		}
	};

	var step_3 = {
		pdf: function (id, serie) {
			//LINK DO PDF
			$.ajax({
				url: '/api/dataentities/LE/search?_fields=list_pdf&id_escola=' + id,
				type: 'GET',
				headers: header
			}).
			done(function (res) {
				$('#btn_link_pdf').attr('href', '#');

				//EXISTE PDFS
				if (res[0].list_pdf) {
					let list = JSON.parse(res[0].list_pdf);

					let url = '';
					$(list).each(function (index, item) {
						if (item.serie === serie) {
							console.log(item.url);
							url += item.url;
						}
					});

					$('#btn_link_pdf').attr('href', url);
				}
			});
		},

		lista_produtos: function (id, ciclo, serie) {
			//LIMPA PRODUTOS
			$('#products li').remove();

			//LISTA OS PRODUTOS DESEJADOS PELO ALUNO
			$.ajax({
				url: '/api/dataentities/LC/search?_fields=ativo,ciclos,name,products,qtd_recomendada,message&id_escola=' + id,
				type: 'GET',
				headers: header
			}).
			done(function (res) {
				console.log(res);

				$(res).each(function (a, b) {
					if (b.ativo === true) {
						let ciclos = JSON.parse(b.ciclos);
						let name_category = b.name;
						let products = b.products;
						let qtd_recomendada = b.qtd_recomendada === '' ? 'Não informado' : b.qtd_recomendada;
						let message = b.message ? 'Observação: ' + b.message : '';

						$.each(ciclos, function (c, d) {
							let series = stringToJson(d.fases);

							//PESQUISA CICLO SELECIONADO
							if (d.name === ciclo) {
								//VERIFICA SE SERIE ESTÁ DISPONIVEL
								//RETORNA PRODUTOS DO CICLO ESCOLHIDO
								if (series[serie] === true) {
									console.log(b);
									functions.build_head_products(name_category, products, qtd_recomendada, message);
								}
							}
						});
					}
				});

				functions.build_content_products();
			});
		},

		seleciona_serie: function () {
			//SELECIONA A SÉRIE
			$(document).on('click', '.lista-escolar.aluno main section[data-step="3"] ul li', function () {

				$('.lista-escolar.aluno main .steps li a[data-step="3"]').addClass('active'); //STEP

				$(this).siblings('li').removeClass('active');
				$(this).addClass('active');

				//ATIVA 3. SÉRIE
				$('.lista-escolar.aluno section[data-step="3"]').fadeOut(300, function () {
					setTimeout(function () {
						$('.lista-escolar.aluno section[data-step="4"]').fadeIn(300);
					}, 300);
				});

				//ID DA ESCOLA
				let id = $('.lista-escolar.aluno main section[data-step="1"]>ul li.active').data('id').toString();

				//CICLO
				let ciclo = $('.lista-escolar.aluno main section[data-step="2"] article ul li.active').data('name');

				//SÉRIE
				let serie = $('.lista-escolar.aluno main section[data-step="3"] ul li.active').data('name');

				//LISTA OS PRODUTOS DESEJADOS PELO ALUNO
				step_3.lista_produtos(id, ciclo, serie);

				//LINK DO PDF
				step_3.pdf(id, serie);
			});
		}
	};

	var functions = {
		toggle: function () {
			$(document).on('click', '#products .toggle', function (e) {
				e.preventDefault();

				$(this).toggleClass('active');
				$(this).parents('li').find('.list').slideToggle();
			});
		},

		ignore: function () {
			$(document).on('click', 'section[data-step="4"] #products .ignore', function (e) {
				e.preventDefault();

				$(this).toggleClass('active');
				$(this).parents('li').toggleClass('ignore');
			});
		},

		search_categoria: function () {
			$('.lista-escolar.aluno section[data-step="4"] #search_categoria').on('keyup', function () {
				let string = $(this).val();

				$('.lista-escolar.aluno #products > li').css('display', 'none');
				$('.lista-escolar.aluno #products > li .head .column_1 a').each(function (index, el) {
					let thisText = $(el).text();
					thisText = thisText.toLowerCase();
					string = string.toLowerCase();

					if (thisText.indexOf(string) != -1) {
						$(el).parents('li').css('display', 'block');
					}
				});
			});
		},

		ordenacao: function () {
			jQuery(document).ready(function ($) {
				$('.lista-escolar.aluno main section[data-step="4"] .ordenacao li a').on('click', function (event) {
					event.preventDefault();

					$('.lista-escolar.aluno main section[data-step="4"] .ordenacao li a').removeClass('active')
					$(this).addClass('active');

					let type = $(this).data('name');

					$('#products .list ul').each(function (index, item) {
						function sortMeBy(arg, sel, elem, order) {
							var $selector = $(sel),
								$element = $selector.children(elem);
							$element.sort(function (a, b) {
								var an = parseFloat(a.getAttribute(arg)),
									bn = parseFloat(b.getAttribute(arg));
								if (order == 'asc') {
									if (an > bn)
										return 1;
									if (an < bn)
										return -1;
								} else if (order == 'desc') {
									if (an < bn)
										return 1;
									if (an > bn)
										return -1;
								}
								return 0;
							});
							$element.detach().appendTo($selector);
						}

						var deferred = sortMeBy('data-price', item, 'li', type);
					});
				});
			});
		},

		//carrinho lateral
		calculate: function () {
			let total = 0;

			$('.carrinho_lateral li .price').each(function (a, b) {
				let valor = Number($(this).attr('data-price'));
				if (!isNaN(valor)) total += valor;
			});

			total = total.toFixed(2);

			$('.carrinho_lateral .total .value').html('R$ ' + total);

			//TOTAL DE ITENS
			$('.carrinho_lateral .list > li').each(function (index, item) {
				$('.carrinho_lateral .head p span').text(index += 1);
			});
		},

		count: function () {
			$(document).on('click', 'section[data-step="4"] .carrinho_lateral .box-count .count', function (e) {
				e.preventDefault();

				let input = $(this).parents('.box-count').find('input');
				let price = parseFloat($(this).parents('.box-count').find('input').attr('data-price'));

				if ($(this).hasClass('count-up')) {
					var qtd_produtos = parseInt(input.attr('value'));
					qtd_produtos++;
				} else if ($(this).hasClass('count-down')) {
					var qtd_produtos = parseInt(input.attr('value'));

					if (qtd_produtos > 1) {
						qtd_produtos--;
					}
				}

				let data_price = price * qtd_produtos;
				data_price = data_price.toFixed(2);

				input.attr('value', qtd_produtos);

				$(this).parents('.ft').find('.price').attr('data-price', data_price);
				$(this).parents('.ft').find('.price').text('R$ ' + data_price);

				setTimeout(() => {
					functions.calculate();
				}, 100);
			});

			$(document).on('click', 'section[data-step="4"] #products .box.qtd a', function (e) {
				e.preventDefault();

				let sku_id = $(this).parents('li').attr('data-sku-id');
				let input = $(this).parents('.box.qtd').find('input');

				if ($(this).hasClass('count_mais')) {
					var qtd_produtos = parseInt(input.attr('value'));
					qtd_produtos++;

					$('section[data-step="4"] .carrinho_lateral>.column ul li[data-sku-id="' + sku_id + '"] .count-up').trigger('click');
				} else if ($(this).hasClass('count_menos')) {
					var qtd_produtos = parseInt(input.attr('value'));

					if (qtd_produtos > 1) {
						qtd_produtos--;

						$('section[data-step="4"] .carrinho_lateral>.column ul li[data-sku-id="' + sku_id + '"] .count-down').trigger('click');
					}
				}

				input.attr('value', qtd_produtos);
			});
		},

		remove: function () {
			$(document).on('click', 'section[data-step="4"] .carrinho_lateral>.column .removeUni', function () {
				$(this).parent('li').remove();

				let id = $(this).parents('li').attr('data-sku-id');
				$('.lista-escolar.aluno main section[data-step="4"] #products>li>.list ul li[data-sku-id="' + id + '"] .remove').trigger('click');

				functions.calculate();
			});

			//#PRODUCTS
			$(document).on('click', '#products li .btn.remove', function (e) {
				e.preventDefault();

				let id = $(this).parents('li').attr('data-sku-id');
				$('.lista-escolar.aluno .carrinho_lateral > .column li[data-sku-id="' + id + '"]').remove();

				$(this).removeClass('active');
				$(this).prev('.buy').addClass('active');

				functions.calculate();
			});
		},

		add: function () {
			$(document).on('click', '#products .buy', function (e) {
				e.preventDefault();

				$(this).removeClass('active')
				$(this).next('.remove').addClass('active');

				let id = $(this).parent('li').data('sku-id');
				let name = $(this).parent('li').find('.name').text();
				let image = $(this).parent('li').find('img').attr('src');

				let listPriceFormated = $(this).parent('li').find('.por').text();
				let data_por = $(this).parent('li').find('.por').attr('data-por');
				let recomendacao = $(this).parent('li').data('recomendacao') === 'Não informado' ? 1 : $(this).parent('li').data('recomendacao');

				let content = '';

				content += '<li data-sku-id="' + id + '">';
				content += '<div class="column_1"><img src="' + image + '"/></div>';

				content += '<div class="column_2">';
				content += '<div class="name">';
				content += '<p>' + name + '</p>';
				content += '</div>';

				content += '<div class="ft">';
				content += '<ul>';
				content += '<li>';
				content += '<div class="box-count">';
				content += '<a href="" class="count count-down">-</a>';
				content += '<input type="text" value="' + recomendacao + '" data-price="' + data_por + '" />';
				content += '<a href="" class="count count-up">+</a>';
				content += '</div>';
				content += '</li>';

				content += '<li>';
				content += '<p class="price" data-price="' + data_por + '">' + listPriceFormated + '</p>';
				content += '</li>';
				content += '<ul>';

				content += '</div>';
				content += '</div>';

				content += '<span class="removeUni">x</span>';
				content += '</li>';

				$('.carrinho_lateral > .column > ul').append(content);

				functions.calculate();
			});
		},

		add_all: function () {
			$('#add_all').on('click', function (e) {
				e.preventDefault();

				$('section[data-step="4"] .carrinho_lateral>.column ul li').remove();
				$('section[data-step="4"] #products > li:not(.ignore) > .list ul li:first-child .buy').trigger('click');
			});
		},

		remove_all: function () {
			$('#remove_all').on('click', function (e) {
				e.preventDefault();

				$('section[data-step="4"] .carrinho_lateral>.column .removeUni').trigger('click');
				$('.lista-escolar.aluno .carrinho_lateral .head p span').text('0');

				functions.calculate();
			});
		},

		finish: function () {
			$(document).on('click', 'section[data-step="4"] .carrinho_lateral .footer .finish', function (e) {
				e.preventDefault();

				$(this).text('Aguarde...');
				$(this).addClass('disabled');

				//produto com grade
				//add multiplos skus
				var arr = [];
				$('.carrinho_lateral > .column > ul > li').each(function (index, x) {
					var int = $(this).attr('data-sku-id');
					var number = $(this).find('.box-count input').val();
					var seller = '1';

					arr.push({
						'sku': int,
						'quantity': number,
						'seller': seller
					});
				});

				var finalArr = arr.reduce((m, o) => {
					var found = m.find(p => p.sku === o.sku);
					if (found) {
						found.quantity += o.quantity;
					} else {
						m.push(o);
					}
					return m;
				}, []);

				var addToCart = {
					index: 0,
					add: {
						products: function (itens, cb) {
							addToCart.products = addToCart.products || [];
							itens = itens[0][0].reverse();
							for (var i in itens) {
								if (itens.hasOwnProperty(i)) {
									addToCart.products.push({
										id: itens[i].sku,
										quantity: itens[i].quantity,
										seller: itens[i].seller
									});
								}
							}
							addToCart.index = addToCart.products.length - 1;
							addToCart.add.product(addToCart.products[addToCart.index], cb);
							return true;
						},
						product: function (item, cb) {
							var adding = false;
							if (typeof (addToCart.products) !== "undefined" && addToCart.index < 0 && typeof (cb) === "function") {
								addToCart.products = [];
								cb();
							}
							if (typeof (item) == "undefined") {
								return false;
							}
							var product = {
								id: item.id,
								quantity: 1 * item.quantity,
								seller: item.seller || 1
							};
							var next = function () {
								addToCart.log('Product id: ' + product.id + ', quantity: ' + product.quantity + ' added.');
								if (typeof (addToCart.products) != "undefined") {
									addToCart.index--;
									addToCart.add.product(addToCart.products[addToCart.index], cb);
								}
							};
							if (!adding) {
								var add = function (prod) {
									var url = '/checkout/cart/add?sku=' + prod.id + '&seller=1&redirect=false&qty=' + prod.quantity;
									adding = true;
									$.get(url, function () {
										adding = false;
										next();
									});
								};
								vtexjs.checkout.getOrderForm().then(function (orderForm) {
									var found = false;
									var items = orderForm.items;
									if (typeof (orderForm) != "undefined" && orderForm.items.length > 0) {
										for (var i in items) {
											if (items.hasOwnProperty(i) && items[i].id == product.id) {
												found = true;
												product.index = items[i].sku,
													product.quantity = items[i].quantity,
													product.seller = items[i].seller;
											} else {
												found = false;
											}
										}
									}

									add(product);
									console.log(product);
									return true;
								});
							}
							return true;
						}
					},
					log: function () {
						if ("undefined" == typeof console && "undefined" == typeof arguments && "undefined" == typeof console.log) {
							return false;
						}
						for (var i in arguments) {
							console.log(arguments[i]);
						}
						return true;
					}
				};
				var addProducts = function (data, cb) {
					addToCart.add.products(data, cb);
					return true;
				};
				var addProduct = function (item, cb) {
					var data = [
						[item.id, item.quantity, item.seller]
					];
					addToCart.add.products(data, cb);
					return true;
				};

				addProducts([
					[finalArr]
				], function () {
					swal("Lista adicionada!", "Os produtos foram adicionados ao carrinho.", "success").then((value) => {
						window.location.href = '/checkout';
					});

					$('section[data-step="4"] .carrinho_lateral .footer .finish').text('Finalizar compra');
					$('section[data-step="4"] .carrinho_lateral .footer .finish').removeClass('disabled');
				});
			});
		},
		//fim - carrinho lateral

		//CONSULTA SKUJSON E CONSTRÓI HTML DAS LISTAS
		build_head_products: function (name_category, products, qtd_recomendada, message) {
			//ABRE A PRIMEIRA CATEGORIA
			setTimeout(function () {
				$(document).find('#products li:first-child .head .column_1 .toggle:not(.active)').trigger('click');
			}, 1000);

			if (products != '') {
				let content = '';
				content += '<li data-name="' + name_category + '" data-list="' + products + '" data-recomendacao="' + qtd_recomendada + '">';
				content += '<div class="head">';
				content += '<div class="column column_1">';
				content += '<a href="#" class="toggle"><span></span> ' + name_category + '</a>';
				content += '</div>';

				content += '<div class="column column_2">';
				content += '<p>Qtd. recomendada: ' + qtd_recomendada + '</p>';
				content += '</div>';

				content += '<div class="column column_3">';
				content += '<p><span></span> Produto adicionado</p>';
				content += '</div>';

				content += '<div class="column column_4">';
				content += '<a href="#" class="ignore"><span></span> Já possuo</a>';
				content += '</div>';
				content += '</div>';

				//INSERE AS LI DE PRODUTOS
				content += '<div class="list slide">';
				content += '<button class="arrow prev"></button>';
				content += '<ul></ul>';
				content += '<button class="arrow next"></button>';
				content += '</div>';
				content += '<p class="message">' + message + '</p>';
				content += '</li>';

				//LISTA DE MATERIAIS
				if (name_category === 'Livro de Atividades' || name_category === 'Livro de Colorir' || name_category === 'Livro de Historias' || name_category === 'Livro Ata' || name_category === 'Livro de Registro') {
					//DEPARTAMENTO DE LIVROS
					$('section[data-step="4"] #products.list_livros').append(content);
				} else {
					$('section[data-step="4"] #products:not(.list_livros)').append(content);
				}
			}
		},

		build_content_products: function () {
			$('#products li').each(function (a, b) {
				let items = $(b).find('.list ul');
				let slide = $(b).find('.list.slide');
				let json = $(b).attr('data-list').split(',');
				let recomendacao = $(b).attr('data-recomendacao');

				$.each(json, function (index, b) {
					//SKUJSON
					$.ajax({
						url: "/api/catalog_system/pub/products/search?fq=productId:" + b,
						type: 'GET',
						headers: header
					}).
					done(function (response) {
						let product = response[0];

						if (product) {
							//APENAS PRODUTOS COM ESTOQUE
							if (product.items[0].sellers[0].commertialOffer.Price != 0) {
								//QUANTIDADE DE PRODUTOS
								slide.attr('data-quantidade', index + 1);

								let image = product.items[0].images ? product.items[0].images[0].imageUrl : '/arquivos/image-not-found.jpg';

								let content = '';
								content += '<li data-id="' + product.productId + '" data-sku-id="' + product.items[0].itemId + '" data-price="' + product.items[0].sellers[0].commertialOffer.Price + '" data-recomendacao="' + recomendacao + '">';
								content += '<figure><img src="' + image + '" data-function="modal" /></figure>';
								content += '<p class="name">' + product.productName + '</p>';
								content += '<div class="price">';
								content += '<p class="de" data-de="' + product.items[0].sellers[0].commertialOffer.Price + '">' + formatReal(product.items[0].sellers[0].commertialOffer.Price) + '</p>';
								content += '<p class="por" data-por="' + product.items[0].sellers[0].commertialOffer.Price + '">' + formatReal(product.items[0].sellers[0].commertialOffer.Price) + '</p>';
								content += '</div>';

								content += '<a href="#" class="btn buy active">Adicionar</a>';
								content += '<a href="#" class="btn remove dis-none">Remover</a>';
								content += '</li>';

								items.append(content);
							}
						}

						//MENORES PREÇOS
						$('.lista-escolar.aluno main section[data-step="4"] .ordenacao a[data-name="asc"]').trigger('click');
					});
				});
			});
		},

		busca_info_ciclo: function (id) {
			//INFORMAÇÕES DO CICLO DA ESCOLA
			$.ajax({
				type: 'GET',
				headers: header,
				url: '/api/dataentities/LC/search?_fields=ciclos&id_escola=' + id
			}).
			done(function (res) {
				console.log(res);

				if (res.length === 0) {
					swal("Oops!", "Sem escola sem produtos.", "warning");
				} else {
					//ATIVA 2. CICLO
					$('.lista-escolar.aluno main .steps li a[data-step="2"]').addClass('active'); //STEP2

					$('.lista-escolar.aluno section[data-step="1"]').fadeOut(300, function () {
						setTimeout(function () {
							$('.lista-escolar.aluno section[data-step="2"]').fadeIn(300);
						}, 300);
					});

					//OCULTA TODOS OS CICLOS
					$('.lista-escolar.aluno main section[data-step="2"] article ul li').addClass('dis-none');

					//PERCORRE OS CICLOS DA ESCOLA SELECIONADA
					$(res).each(function (a, b) {
						let ciclos = stringToJson(b.ciclos);

						//SELECIONA CICLOS COM DISPONIBILIDADE - ALL TRUE
						$(ciclos).each(function (c, d) {

							let fases = stringToJson(d.fases);

							$.each(fases, function (key, value) {
								if (value === true) {
									$('.lista-escolar.aluno main section[data-step="2"] article ul li[data-name="' + d.name + '"]').removeClass('dis-none');
								}
							});
						});
					});
				}
			});
			//INFORMAÇÕES DO CICLO DA ESCOLA
		},

		return: function () {
			$('.lista-escolar.aluno main section[data-step="2"] .return').on('click', function (e) {
				e.preventDefault();

				$('.lista-escolar.aluno section[data-step="2"]').fadeOut(300, function () {
					setTimeout(function () {
						$('.lista-escolar.aluno section[data-step="1"]').fadeIn(300);
					}, 300);
				});
			});

			$('.lista-escolar.aluno main section[data-step="3"] .return').on('click', function (e) {
				e.preventDefault();

				$('.lista-escolar.aluno section[data-step="3"]').fadeOut(300, function () {
					setTimeout(function () {
						$('.lista-escolar.aluno section[data-step="2"]').fadeIn(300);
					}, 300);
				});
			});

			$('.lista-escolar.aluno main section[data-step="4"] .return').on('click', function (e) {
				e.preventDefault();

				$('.lista-escolar.aluno section[data-step="4"]').fadeOut(300, function () {
					setTimeout(function () {
						$('.lista-escolar.aluno section[data-step="3"]').fadeIn(300);
					}, 300);
				});
			});
		}
	};

	if ($('body').hasClass('lista-escolar aluno')) {
		step_1.busca_escola();
		step_1.busca_info();

		step_2.busca_serie();

		step_3.seleciona_serie();

		functions.toggle();
		functions.ignore();
		functions.search_categoria();
		functions.ordenacao();
		functions.count();
		functions.remove();
		functions.add();
		functions.add_all();
		functions.remove_all();
		functions.finish();
		functions.return();
	}

})();

var cliente = (function () {
	var functions = {
		logado: function () {
			$.ajax({
				url: '/api/vtexid/pub/authenticated/user',
				type: 'GET'
			}).done(function (user) {
				//SUPORTE = NÃO ESTÁ LOGADO
				if (user === null || user.user === 'suporte@sandersdigital.com.br') {
					console.log('O cliente não está logado.');

					//EMAIL CLIENTE
					localStorage.removeItem('email_cliente');
				} else {
					console.log('O cliente está logado.');

					//EMAIL CLIENTE
					localStorage.setItem('email_cliente', user.user);

					$.ajax({
							url: '/api/dataentities/CL/search?_fields=email,firstName,userId&email=' + user.user,
							type: 'GET',
							dataType: 'json'
						})
						.done(function (res) {
							// HEADER
							if (res[0]) {
								$('header #client_name').text(res[0].firstName);
								$('header #logout').removeClass('dis-none');

								//LISTA-ESCOLAR/CADASTRO
								$('#criar_escola input[name="first_email"]').attr('value', res[0].email);
							}
						});
				}
			}).fail(function (user) {
				console.log('O cliente não está logado.');

				//EMAIL CLIENTE
				localStorage.removeItem('email_cliente');
			});
		}
	}

	functions.logado();
})();

var carrinho = (function () {
	var desktop = {
		toggle_carrinho: function () {
			$('body:not(.lista-escolar) header .row-2 li.cart a').on('click', function (event) {
				event.preventDefault();
				$('#cart-lateral, #overlay').addClass('active');
			});

			$('#cart-lateral .header .close').on('click', function (event) {
				event.preventDefault();
				$('#cart-lateral, #overlay').removeClass('active');
			});
		},

		calculateShipping: function () {
			if ($('#search-cep input[type="text"]').val() != '') {
				vtexjs.checkout.getOrderForm()
					.then(function (orderForm) {
						if (localStorage.getItem('cep') === null) {
							var postalCode = $('#search-cep input[type="text"]').val();
						} else {
							var postalCode = localStorage.getItem('cep');
						}

						var country = 'BRA';
						var address = {
							"postalCode": postalCode,
							"country": country
						};

						return vtexjs.checkout.calculateShipping(address)
					})
					.done(function (orderForm) {
						if (orderForm.totalizers.length != 0) {
							var value_frete = orderForm.totalizers[1].value / 100;
							value_frete = value_frete.toFixed(2).replace('.', ',').toString();
							$('#cart-lateral .value-frete').text('R$: ' + value_frete);

							var postalCode = $('#search-cep input[type="text"]').val();
							localStorage.setItem('cep', postalCode);
							$('#search-cep input[type="text"]').val(postalCode);
						}
					});
			}
		},

		//APOS INSERIDO - CALCULA AO CARREGAR A PG
		automaticCalculateShipping: function () {
			$(window).load(function () {
				if (localStorage.getItem('cep') != null && localStorage.getItem('cep') != 'undefined') {
					$('#search-cep input[type="text"]').val(localStorage.getItem('cep'));
					desktop.calculateShipping();
				}
			});
		},

		//CALCULA MANUALMENTE
		calculaFrete: function () {
			//MASK
			$('#search-cep input[type="text"]').mask('00000-000');

			//CLICK
			$('#search-cep input[type=submit]').on('click', function (e) {
				e.preventDefault();
				if ($('#search-cep input[type="text"]').val().length === 9) {
					desktop.calculateShipping();
					$('#search-cep input[type="text"]').removeClass('active');
				} else {
					$('#search-cep input[type="text"]').addClass('active');
				}
			});

			//PRESS ENTER
			$('#search-cep input[type=text]').on('keypress', function (event) {
				if (keycode == '13') {
					if ($('#search-cep input[type="text"]').val().length === 9) {
						var keycode = (event.keyCode ? event.keyCode : event.which);
						desktop.calculateShipping();
						$('#search-cep input[type="text"]').removeClass('active');
					} else {
						$('#search-cep input[type="text"]').addClass('active');
					}
				}
			});
		},

		cartLateral: function () {
			vtexjs.checkout.getOrderForm()
				.done(function (orderForm) {
					//TOTAL CARRINHO
					var quantidade = 0;
					for (var i = orderForm.items.length - 1; i >= 0; i--) {
						quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
					}

					$('header .row-2 li.cart span').text(quantidade);

					//INFORMACOES DO CARRINHO
					if (orderForm.value != 0) {
						total_price = orderForm.value / 100;
						total_price = total_price.toFixed(2).replace('.', ',').toString();

						$('#cart-lateral .footer .total-price').text('R$: ' + total_price);
					} else {
						$('#cart-lateral .footer .total-price').text('R$: 0,00');
					}

					if (orderForm.totalizers.length != 0) {
						sub_price = orderForm.totalizers[0].value / 100;
						sub_price = sub_price.toFixed(2).replace('.', ',').toString();

						$('#cart-lateral .footer .value-sub-total, #cart-lateral .header .value-sub-total').text('R$: ' + sub_price);
					} else {
						$('#cart-lateral .footer .value-sub-total, #cart-lateral .header .value-sub-total').text('R$: 0,00');
					}

					if (orderForm.items != 0) {
						total_items = orderForm.items.length;

						$('#cart-lateral .header .total-items span').text(total_items + ' Itens');
					} else {
						$('#cart-lateral .header .total-items span').text('0 Itens');
					}
					//FIM - INFORMACOES DO CARRINHO

					//ITEMS DO CARRINHO
					$('#cart-lateral .content ul li').remove();
					for (i = 0; i < orderForm.items.length; i++) {

						price_item = orderForm.items[i].price / 100;
						price_item = price_item.toFixed(2).replace('.', ',').toString();

						var content = '';

						content += '<li data-index="' + i + '">';
						content += '<div class="column_1"><img src="' + orderForm.items[i].imageUrl + '" alt="' + orderForm.items[i].name + '"/></div>';

						content += '<div class="column_2">';
						content += '<div class="name">';
						content += '<p>' + orderForm.items[i].name + '</p>';
						content += '</div>';

						content += '<div class="ft">';
						content += '<ul>';
						content += '<li data-index="' + i + '">';
						content += '<div class="box-count">';
						content += '<a href="" class="count count-down">-</a>';
						content += '<input type="text" value="' + orderForm.items[i].quantity + '" />';
						content += '<a href="" class="count count-up">+</a>';
						content += '</div>';
						content += '</li>';

						content += '<li class="price">';
						content += '<p>R$: ' + price_item + '</p>';
						content += '</li>';
						content += '<ul>';

						content += '</div>';
						content += '</div>';

						content += '<span class="removeUni">x</span>';
						content += '</li>';

						$('#cart-lateral .content > ul').append(content);
					}
					//FIM - ITEMS DO CARRINHO
				});
		},

		changeQuantity: function () {
			$(document).on('click', '#cart-lateral .count', function (e) {
				e.preventDefault();

				var qtd = $(this).siblings('input[type="text"]').val();
				if ($(this).hasClass('count-up')) {
					qtd++
					$(this).siblings('input[type="text"]').removeClass('active');
					$(this).siblings('input[type="text"]').val(qtd);
				} else if ($(this).hasClass('count-down')) {
					if ($(this).siblings('input[type="text"]').val() != 1) {
						qtd--
						$(this).siblings('input[type="text"]').val(qtd);
					} else {
						//ALERTA 0 USUARIO QUANTIDADE NEGATIVA
						$(this).siblings('input[type="text"]').addClass('active');
					}
				}

				var data_index = $(this).parents('li').data('index');
				var data_quantity = $(this).parents('li').find('.box-count input[type="text"]').val();

				vtexjs.checkout.getOrderForm()
					.then(function (orderForm) {
						var total_produtos = parseInt(orderForm.items.length);
						vtexjs.checkout.getOrderForm()
							.then(function (orderForm) {
								var itemIndex = data_index;
								var item = orderForm.items[itemIndex];

								var updateItem = {
									index: data_index,
									quantity: data_quantity
								};

								return vtexjs.checkout.updateItems([updateItem], null, false);
							})
							.done(function (orderForm) {
								desktop.cartLateral();
							});
					});
			});
		},

		removeItems: function () {
			$(document).on('click', '#cart-lateral .removeUni', function () {

				var data_index = $(this).parents('li').data('index');
				var data_quantity = $(this).siblings('li').find('.box-count input[type="text"]').val();

				vtexjs.checkout.getOrderForm()
					.then(function (orderForm) {
						var itemIndex = data_index;
						var item = orderForm.items[itemIndex];
						var itemsToRemove = [{
							"index": data_index,
							"quantity": data_quantity,
						}]
						return vtexjs.checkout.removeItems(itemsToRemove);
					})
					.done(function (orderForm) {
						desktop.cartLateral();
					});
			});
		},

		removeAllItems: function () {
			$('#cart-lateral .clear').on('click', function () {
				vtexjs.checkout.removeAllItems()
					.done(function (orderForm) {
						//ATUALIZA O CARRINHO APÓS ESVAZIAR
						desktop.cartLateral();
					});
			});
		},

		btn_buy: function () {
			window.alert = function () {
				//OPEN CART
				$('header .row-2 li.cart a').trigger('click');
				desktop.cartLateral();
			}
		},

		openCart: function () {
			$('#overlay').on('click', function () {
				if ($('#cart-lateral').hasClass('active')) {
					$('#cart-lateral, #overlay').removeClass('active');
				}
			});
		}
	};

	var mobile = {
		toggle_carrinho: function () {
			$('#cart-lateral .header').on('click', function () {
				$('#cart-lateral, #overlay').toggleClass('active');
			});
		}
	};

	var prateleira = {
		brand: function () {
			$('.prateleira ul li').each(function (a, b) {
				let brand_image = $(b).find('figure i > img');
				brand_image.attr('src', '/arquivos/' + $(b).find('figure i .brand').text() + '.png');
			});
		},

		buy_button: function () {
			$('.prateleira ul li article .buy_button').on('click', function (e) {
				e.preventDefault();

				$(this).addClass('loading');

				vtexjs.catalog.getProductWithVariations($(this).parents('article').data('id')).done(function (product) {
					console.log(product);

					let item = {
						id: product.skus[0].sku,
						quantity: 1,
						seller: '1'
					};

					vtexjs.checkout.getOrderForm()
						.done(function (orderForm) {
							vtexjs.checkout.getOrderForm()
							vtexjs.checkout.addToCart([item], null, 1).done(function (orderForm) {

								$('.prateleira ul li article .buy_button').removeClass('loading');

								//OPEN CART
								$('#cart-lateral, #overlay').addClass('active');
								desktop.cartLateral();
							});
						});
				});
			});
		}
	};

	var departamento = {
		smart_research: function () {
			$(".search-multiple-navigator input[type='checkbox']").vtexSmartResearch({
				shelfCallback: function () {
					console.log('shelfCallback');
				},

				ajaxCallback: function () {
					prateleira.brand();
					prateleira.buy_button();
				}
			});
		}
	};

	if ($('body').width() < $mobile) {
		mobile.toggle_carrinho();
	} else {
		desktop.toggle_carrinho();
	}

	desktop.automaticCalculateShipping();
	desktop.calculaFrete();
	desktop.cartLateral();
	desktop.changeQuantity();
	desktop.removeItems();
	desktop.removeAllItems();
	desktop.btn_buy();
	desktop.openCart();

	prateleira.brand();
	prateleira.buy_button();

	if ($('body').hasClass('departamento')) {
		departamento.smart_research();
	}
})();

var geral = (function () {
	var functions = {
		search: function () {
			$('header .search input').on('input', function (event) {
				texto = $(this).val();

				if (texto != '') {
					$('header .search .result').addClass('active loading');

					$.ajax({
							url: '/api/catalog_system/pub/products/search/' + texto,
							type: 'GET',
							dataType: 'json'
						})
						.done(function (success) {

							var busca = '';

							$(success).each(function (index, a) {
								//SHOW CATEGORY
								let categorias = a.categories[1].replace(/[0-9`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
								$('.digitando').html('<p>' + texto + '</p>' + '<p>' + texto + '<a href="' + categorias + '"> - ' + categorias + '</a></p>');

								vtexjs.catalog.getProductWithVariations(a.productId).done(function (product) {
									//PRODUTO EM ESTOQUE
									if (product.available === true) {
										$(product.skus).each(function (index, b) {
											//VARIACAO EM ESTOQUE
											if (b.available === true) {
												console.log(b);
												var ID = a.productId;
												var nome = a.productName;
												var foto = a.items[0].images[0].imageUrl;
												var url = a.link;

												//BUILD PRICE
												var de_price = b.listPriceFormated;
												var por_price = b.bestPrice;
												var number_parcelas = b.installments;
												var value_parcelas = b.installmentsValue;
												value_parcelas = value_parcelas / 100;
												value_parcelas = value_parcelas.toFixed(2);
												value_parcelas = value_parcelas.replace('.', ',');

												if (por_price != 0) {
													busca += '<li data-id=' + ID + '>';
													busca += '<a href="' + url + '">';
													busca += '<div class="image"><img src="' + foto + '" /></div>';
													busca += '<div class="name"><p>' + nome + '</p></div>';

													if (de_price != por_price) {
														busca += '<div class="box_price">';
														if (b.listPrice != 0) {
															busca += '<p class="de_price preco">De: <span>' + b.listPriceFormated + '</span></p>';
														}
														busca += '<p class="por_price preco">Por: <span>R$ ' + b.bestPriceFormated + '</span></p>';
														busca += '<p class="parcelas">em até <span>' + number_parcelas + 'x</span> de <span>R$ ' + value_parcelas + '</span></p>';
														busca += '</div>';
													} else {
														busca += '<div class="box_price">';
														busca += '<p class="por_price preco">Por: <span>R$ ' + b.bestPriceFormated + '</span></p>';
														busca += '</div>';
													}

													busca += '</li>';

													$('header .search .result ul').html(busca);
												}
											}
										});
									}
								});

								if ($(a).length === index + 1) {
									console.log('finish')
									$('header .search .result').removeClass('loading');
								}
							});
						});
				} else {
					$('header .search ul li, header .search .digitando p').remove();
					$('header .search .result').removeClass('active');
				}
			});

			//CLICK BUTTON
			$("header .search button").on('click', function () {
				var texto = $('header .search input').val();
				location.href = '/' + texto;
			});

			//PRESS ENTER
			$('header .search input').keypress(function (event) {
				var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode == 13) {
					location.href = '/' + $(this).val();
				}
			});
		},

		newsletter: function () {
			$('#newsletterButtonOK').val('cadastrar');
		}
	}

	var desktop = {
		banner_principal: function () {
			$('.lista-escolar .fullbanner ul, .home .fullbanner ul').slick({
				infinite: false,
				autoplay: true,
				autoplaySpeed: 4000,
				arrows: true,
				dots: true,
				slidesToShow: 1,
				slidesToScroll: 1
			});

			$('.home .fullbanner.desktop').removeClass('loading');
		}
	};

	var mobile = {
		hamburger: function () {
			$('.hamburger').on('click', function () {
				$('header nav').toggleClass('active');
			});

			$('header .row-3 nav .close').on('click', function () {
				$('.hamburger').trigger('click');
			});
		},

		header_toggle: function () {
			$('header .row-3 nav ul li p a').on('click', function (e) {
				//existe submenu?
				if ($(this).parents('li').find('.submenu').length) {
					e.preventDefault();

					if ($(this).parents('li').hasClass('active')) {
						$(this).parents('li').removeClass('active')
						$(this).parents('li').find('.submenu').slideUp();
					} else {
						$('header .row-3 nav .content ul li').removeClass('active');
						$('header .row-3 nav .content ul li div.submenu').slideUp();

						$(this).parents('li').addClass('active')
						$(this).parents('li').find('.submenu').slideDown();
					}
				}
			});
		},

		footer_toggle: function () {
			$('.toggle .title').on('click', function () {
				$(this).toggleClass('active');
				$(this).next('ul').slideToggle();
			});
		}
	}

	functions.search();
	functions.newsletter();

	desktop.banner_principal();

	if ($('body').width() < $mobile) {
		mobile.hamburger();
		mobile.header_toggle();
		mobile.footer_toggle();
	}
})();

var home = (function () {
	var desktop = {
		marcas: function () {
			$('.home .marcas ul').slick({
				infinite: true,
				autoplay: true,
				autoplaySpeed: 4000,
				arrows: true,
				dots: false,
				slidesToShow: 6,
				slidesToScroll: 1,
				responsive: [{
						breakpoint: 1024,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 600,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					}
				]
			});
		},

		one_prod: function () {
			$('.one_prod .prateleira').addClass('one');
			$('.home .prateleira.one ul').slick({
				infinite: false,
				arrows: true,
				dots: false,
				slidesToShow: 1,
				slidesToScroll: 1
			});
		},

		prateleira: function () {
			$('.home .prateleira:not(.one) ul').slick({
				infinite: false,
				arrows: true,
				dots: false,
				slidesToShow: 4,
				slidesToScroll: 1
			});
		},
	}

	var mobile = {
		navegue: function () {
			$('.home .navegue ul').slick({
				infinite: false,
				autoplay: true,
				autoplaySpeed: 4000,
				arrows: false,
				dots: false,
				slidesToShow: 2,
				slidesToScroll: 1
			});
		},

		banner_category: function () {
			$('.home .banner_category, .home .tipbar').slick({
				infinite: false,
				autoplay: true,
				autoplaySpeed: 4000,
				arrows: false,
				dots: false,
				slidesToShow: 1,
				slidesToScroll: 1
			});
		}
	}

	if ($('body').hasClass('home')) {
		desktop.marcas();

		if ($('body').width() < $mobile) {
			mobile.navegue();
			mobile.banner_category();
		} else {
			desktop.one_prod();
			desktop.prateleira();
		}
	}
})();

var departamento = (function () {
	var desktop = {
		busca_custom: function () {
			var buscaFiltro =
				'<article class="busca_filtro">' +
				'<input type="text" id="buscaFiltro" placeholder="Busque por palavra-chave" />' +
				'</article>';

			$(window).load(function () {
				$(document).find('.filtro_tipos-de-tecidos h5').after(buscaFiltro);
			});

			$(document).on('keyup', '#buscaFiltro', function () {
				var string = '';
				var string = $(this).val();

				$('.search-multiple-navigator .filtro_tipos-de-tecidos label').css('display', 'none');
				$('.search-multiple-navigator .filtro_tipos-de-tecidos label').each(function (index, el) {
					var thisText = $(el).text();
					thisText = thisText.toLowerCase();
					string = string.toLowerCase();

					if (thisText.indexOf(string) != -1) {
						$(el).css('display', 'flex');
					}
				});
			});
		},

		sugestions: function () {
			var list = [];

			for (let i = 0; i < $('.prateleira ul li article').length; i++) {
				const element = $('.prateleira ul li article')[i];
				var name = $(element).attr('data-category');
				var link = $(element).attr('data-link');

				list.push({
					name: name,
					link: link
				});
			}

			function getUnique(arr, comp) {
				const unique = arr
					.map(e => e[comp])

					// store the keys of the unique objects
					.map((e, i, final) => final.indexOf(e) === i && i)

					// eliminate the dead keys & store unique objects
					.filter(e => arr[e]).map(e => arr[e]);

				return unique;
			}

			var new_list = getUnique(list, 'name');

			for (let i = 0; i < new_list.length; i++) {
				const element = new_list[i];

				$('.sugestions ul').append('<li><a href="' + element.link + '" title="' + element.name + '">' + element.name + '</a></li>');
			}
		},

		ordernar_por: function () {
			$('.ordenar_por .content ul li label').on('click', function (e) {
				e.preventDefault();

				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
					$('.search-multiple-navigator fieldset div label input[rel="' + $(this).find('input').data('value') + '"]').trigger('click');
				} else {
					$('.ordenar_por .content ul li label').removeClass('active');
					$('.search-multiple-navigator fieldset div label input[rel="' + $('.ordenar_por .content ul li label.active input').data('value') + '"]').trigger('click');

					$(this).addClass('active');
					$('.search-multiple-navigator fieldset div label input[rel="' + $(this).find('input').data('value') + '"]').trigger('click');
				}
			});
		},

		flags: function () {
			//ADD
			$(document).on('change', '.search-multiple-navigator label input', function () {
				var thisName = $(this).parent().text(),
					thisClass = $(this).parent().attr('title'),
					categoriaSelecionada = '<li data-name="' + thisClass + '"><p>' + thisName + '</p><span>x</span></li>';

				if ($(this).parent().hasClass('sr_selected')) {
					$('.flags ul').append(categoriaSelecionada);
				} else {
					$('.flags .content ul li[data-name="' + thisClass + '"]').remove();
				}
			});

			//REMOVE
			$(document).on('click', '.flags li', function (e) {
				e.preventDefault();
				$('.search-multiple-navigator label[title="' + $(this).data('name') + '"]').trigger('click');
			});

			//CLEAR
			$('.flags .clear a').on('click', function (e) {
				e.preventDefault();
				$('.flags .content ul li').trigger('click')
			});
		},

		toggle: function () {
			$('.search-single-navigator h4 a').on('click', function (e) {
				e.preventDefault();
				$(this).toggleClass('active');
				$(this).parents('h4').next('ul').slideToggle();
			});
		}
	};

	var mobile = {
		ordenar: function () {
			$('.filter_buttons a[title="Ordenar"]').on('click', function (e) {
				e.preventDefault();
				$('.ordenar_por').toggleClass('active');
			});

			//FECHAR E APLICAR
			$('.ordenar_por .title, .ordenar_por .btn_aplicar').on('click', function (e) {
				e.preventDefault();
				$('.filter_buttons a[title="Ordenar"]').trigger('click');
			});
		},

		filtrar: function () {
			$('.filter_buttons a[title="Filtrar"]').on('click', function (e) {
				e.preventDefault();
				$('.search_navigator').toggleClass('active');
			});

			//FECHAR E APLICAR
			$('.search_navigator .head, .search_navigator .btn_aplicar').on('click', function (e) {
				e.preventDefault();
				$('.filter_buttons a[title="Filtrar"]').trigger('click');
			});
		}
	};

	if ($('body').hasClass('departamento')) {
		desktop.busca_custom();
		desktop.sugestions();
		desktop.flags();
		desktop.ordernar_por();
		desktop.toggle();

		if ($('body').width() < $mobile) {
			mobile.ordenar();
			mobile.filtrar();
		}
	}
})();

var produto = (function () {
	var ambos = {
		specification: function () {
			//FICHA TÉCNICA
			$('.produto .product_details .content_2 .text p').text($('.value-field.Ficha-Tecnica').text());
		},

		saiba_mais: function () {
			$('.produto main .row_2 .product_description a').on('click', function (e) {
				e.preventDefault();
				$(this).remove();
				$('.produto main .row_2 .product_description .text').addClass('active');
			});
		},

		tags: function () {
			let first = $('.bread-crumb ul li:nth-child(2)').text().toLowerCase().replace(' ', '-');
			let second = $('.bread-crumb ul li:nth-child(3)').text().toLowerCase().replace(' ', '-');

			$.ajax({
				url: 'https://dk10.com.br/fernet/add_site/connect-api.php',
				dataType: 'json',
				method: 'GET',
				action: 'GET',
				data: {
					parametro: 'https://fernet.vtexcommercestable.com.br/api/catalog_system/pub/facets/search/' + first + '/' + second + '?map=c,c'
				},
				success: function (response) {
					console.log(response);

					// $(response.SpecificationFilters['Material']).each(function (a,b){
					// 	$('.produto .product_details .content_1 .tags').append('<li><a href="#">' +b.Name+ '</a></li>');
					// });

					// $(response.SpecificationFilters['Tamanho']).each(function (a,b){
					// 	$('.produto .product_details .content_1 .tags').append('<li><a href="#">' +b.Name+ '</a></li>');
					// });
				}
			});
		},

		prateleira: function () {
			if ($('body').width() < $mobile) {
				$('.produto .prateleira ul').each(function (a, b) {
					if ($(b).find('li').length > 1) {
						$(b).slick({
							autoplay: true,
							autoplaySpeed: 4000,
							arrows: true,
							dots: false,
							slidesToShow: 1,
							slidesToScroll: 1
						});
					}
				});
			} else {
				$('.produto .prateleira ul').each(function (a, b) {
					if ($(b).find('li').length > 4) {
						$(b).slick({
							autoplay: true,
							autoplaySpeed: 4000,
							arrows: true,
							dots: false,
							slidesToShow: 4,
							slidesToScroll: 1
						});
					}
				});
			}
		},

		count: function () {
			$('.produto .product_cta .qtd span').on('click', function (e) {
				e.preventDefault();
				if ($(this).hasClass('count-mais')) {
					var qtd_produtos = $('.produto .product_cta .qtd input').attr('value');
					qtd_produtos++;
				} else if ($(this).hasClass('count-menos')) {
					var qtd_produtos = $('.produto .product_cta .qtd input').attr('value');

					if (qtd_produtos > 1) {
						qtd_produtos--;
					}
				}

				$('.produto .product_cta .qtd input').attr('value', qtd_produtos);

				let qty = parseInt(qtd_produtos);
				$('.buy-in-page-quantity').trigger('quantityChanged.vtex', [skuJson.productId, qty]);
			});
		},

		thumbs: function () {
			$('.produto .thumbs').slick({
				infinite: true,
				autoplay: false,
				arrows: true,
				dots: true,
				slidesToShow: 1,
				slidesToScroll: 1
			});

			$('.thumbs .slick-slide:not(.slick-cloned)').each(function (i, b) {
				let image = $(b).find('img').attr('src');

				$('.apresentacao .slick-dots li button[id="slick-slide-control0' + i + '"]').css('background-image', 'url(' + image + ')');
				$('.apresentacao .slick-dots li button[id="slick-slide-control1' + i + '"]').css('background-image', 'url(' + image + ')');
			});

			$('.thumbs .slick-slide').zoom();
		},

		details: function () {
			$('.produto .product_details>ul li a').on('click', function (e) {
				e.preventDefault();

				let id = $(this).data('id');

				$('.produto main .product_details .content, .produto .product_details>ul li a').removeClass('active');
				$('.produto main .product_details .content_' + id).addClass('active');
				$(this).addClass('active');
			});
		}
	}

	var mobile = {
		details: function () {
			$('.produto .product_details .box .content .title').on('click', function (e) {
				e.preventDefault();

				$('.produto main .product_details .box .content').removeClass('active');
				$(this).parents('.content').toggleClass('active');
			});
		}
	}

	if ($('body').hasClass('produto')) {
		ambos.saiba_mais();
		ambos.tags();
		ambos.prateleira();
		ambos.count();
		ambos.thumbs();
		ambos.details();

		if ($('body').width() < $mobile) {
			mobile.details();
		}
	}
})();

var institucional = (function () {
	var functions = {
		busca: function () {
			$(".error form input").keypress(function (event) {
				var texto = $(this).val();
				location.href = '/' + texto;
			})

			$(".error form .button-search").on('click', function (event) {
				e.preventDefault();

				var texto = $(".error form input").val();
				location.href = '/' + texto
			})
		},

		atendimento: function () {
			$('.institucional.atendimento input[type="submit"]').on('click', function (e) {
				e.preventDefault();

				var dados = {
					nome: $('input[name="nome"]').val(),
					email: $('input[name="email"]').val(),
					telefone: $('input[name="telefone"]').val(),
					assunto: $('select option:selected').text(),
					mensagem: $('textarea').val()
				}

				var json_dados = JSON.stringify(dados);
				console.log(json_dados);

				insertMasterData("CT", 'fernet', json_dados, function (res) {
					console.log(res);
					$('.box-form form').remove();
					$('.box-form').append('<div style="text-align: center;">Sua mensagem foi enviada com sucesso! <br> Retornaremos em breve! </div>');
				});
			});
		},

		random: function () {
			$(window).on('load', function () {

				//menu
				var titlePag = $('h2.title').text();
				var listMenu = $('.menu-institucional ul li').length;
				var li = '';
				var liMob = "<li><a>Menu Institucional</a></li>";

				$('.menu-institucional ul li a.active').removeClass('active');

				for (i = 1; i < 8; i++) {
					li = $('.menu-institucional ul li:nth-child(' + i + ') a').text();
					if (li == titlePag) {
						$('.menu-institucional ul li:nth-child(' + i + ') a').addClass('active');
					}
				}

				if (window.outerWidth <= 769) {
					$('.menu-institucional ul').prepend(liMob);

					$('.menu-institucional ul').on('click', function () {
						$('.menu-institucional ul').toggleClass('active');
					});
				}
			});
		}
	}

	functions.busca();
	functions.random();
	functions.atendimento();
})();